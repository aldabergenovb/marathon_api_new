using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using pmo_api.Constants;
using pmo_api.Helpers;
using pmo_api.Repositories;
using pmo_api.Models.Directories;
using pmo_api.Models.Filters;
using pmo_api.Models.MarathonUserss;
using System.Collections.Generic;

namespace pmo_api.Controllers
{
    [ApiExplorerSettings(IgnoreApi = false)]
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class MarathonUsersController : Controller
    {
        private readonly string _lang;
        private readonly IMarathonUsersRepository _marathonUserss;

        public MarathonUsersController(IHttpContextAccessor httpContextAccessor, IMarathonUsersRepository marathonUserss)
        {
            _lang = httpContextAccessor.HttpContext.Items["ClientLang"].ToString();
            _marathonUserss = marathonUserss;
        }

        /// <summary>
        /// Получение данных по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            ActionMethodResult result;
            if (!id.Equals(Guid.Empty))
                result = await _marathonUserss.GetAsync(id, User, _lang);
            else
                result = ActionMethodResult.Error(ActionMethodConstants.Results.InvalidData, "", MessageHelper.InvalidData(_lang));
            return Ok(result);
        }

        [HttpGet("PaginationAdmin")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> GetAllPaginated([FromQuery] MarathonUsersFilters options)
        {
            ActionMethodResult result;
            if (ModelState.IsValid)
                result = await _marathonUserss.GetAllPaginatedAsync(options, User, _lang);
            else
                result = ActionMethodResult.Error(ActionMethodConstants.Results.InvalidData, "", MessageHelper.InvalidData(_lang));
            return Ok(result);
        }

        [HttpGet("PaginationMain")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IActionResult> GetAllPaginatedMainList([FromQuery] MarathonUsersFilters options)
        {
            ActionMethodResult result;
            if (ModelState.IsValid)
                result = await _marathonUserss.GetAllPaginatedMainListAsync(options, User, _lang);
            else
                result = ActionMethodResult.Error(ActionMethodConstants.Results.InvalidData, "", MessageHelper.InvalidData(_lang));
            return Ok(result);
        }

        /// <summary>
        /// Создание новой записи
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("create")]
        [AllowAnonymous]
        public async Task<IActionResult> CreateMarathonUsers([FromBody] MarathonUsersCreateViewModel model)
        {
            ActionMethodResult result;
            if (ModelState.IsValid)
                result = await _marathonUserss.CreateAsync(model, User, _lang);
            else
                result = ActionMethodResult.Error(ActionMethodConstants.Results.InvalidData, "", MessageHelper.InvalidData(_lang));
            return Ok(result);
        }

        /// <summary>
        /// Обновление записи
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("put")]
        public async Task<IActionResult> PutAsync([FromBody] MarathonUsersPutViewModel model)
        {
            ActionMethodResult result;
            if (ModelState.IsValid)
                result = await _marathonUserss.PutAsync(model, User, _lang);
            else
                result = ActionMethodResult.Error(null, MessageHelper.InvalidData(_lang));
            return Ok(result);
        }

        /// <summary>
        /// Удаление записи
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            ActionMethodResult result;
            if (ModelState.IsValid)
                result = await _marathonUserss.DeleteAsync(id, User, _lang);
            else
                result = ActionMethodResult.Error(null, MessageHelper.InvalidData(_lang));
            return Ok(result);
        }

        /// <summary>
        /// Удаление списка записей
        /// </summary>
        /// <param name="idList"></param>
        /// <returns></returns>
        [HttpDelete("list")]
        public async Task<IActionResult> DeleteListAsync([FromQuery] List<Guid> idList)
        {
            ActionMethodResult result;
            if (ModelState.IsValid)
                result = await _marathonUserss.DeleteListAsync(idList, User, _lang);
            else
                result = ActionMethodResult.Error(null, MessageHelper.InvalidData(_lang));
            return Ok(result);
        }
    }
}