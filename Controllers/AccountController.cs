using System.Threading.Tasks;
using pmo_api.Constants;
using pmo_api.Helpers;
using pmo_api.Models.Base;
using pmo_api.Models.Directories;
using pmo_api.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace pmo_api.Controllers
{
    [ApiExplorerSettings(IgnoreApi = false)]
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountRepository _account;
        private readonly string _lang;

        public AccountController(IAccountRepository account, IHttpContextAccessor httpContextAccessor)
        {
            _account = account;
            _lang = httpContextAccessor.HttpContext.Items["ClientLang"].ToString();
        }

        /// <summary>
        /// Аутентификация пользователя
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync([FromBody] LoginForm model)
        {
            if (!ModelState.IsValid)
                return Ok(ActionMethodResult.Error(ActionMethodConstants.Results.ServerError, "", MessageHelper.ServerError(_lang)));

            var result = await _account.LoginAsync(model, _lang);
            return Ok(result);
        }
    }
}