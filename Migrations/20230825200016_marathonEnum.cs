﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace pmo_api.Migrations
{
    public partial class marathonEnum : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "marathonEnum",
                table: "MarathonUsers",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "marathonEnum",
                table: "MarathonUsers");
        }
    }
}
