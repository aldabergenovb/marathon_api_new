using System;
using System.Linq;
using pmo_api.Constants;
using pmo_api.Models.Filters;
using pmo_api.Models.MarathonUserss;
using Microsoft.EntityFrameworkCore;
using pmo_api.Models.Enums;

namespace pmo_api.Helpers
{
    public static class MarathonUsersHelper
    {
        public static IQueryable<MarathonUsers> ApplyRoleFilter(this IQueryable<MarathonUsers> source, string role, Guid userId)
        {
            return role switch
            {
                UserConstants.Roles.Admin or UserConstants.Roles.Root or UserConstants.Roles.System => source,
                _ => source,
                //_ => source.Where(x => x.Id == Guid.Empty),
            };
        }

        public static IQueryable<MarathonUsers> ApplyFilters(this IQueryable<MarathonUsers> source, MarathonUsersFilters options)
        {
            if (options.CreatedBefore != null)
                source = source.Where(x => x.Created >= options.CreatedBefore);

            if (options.CreatedAfter != null)
                source = source.Where(x => x.Created <= options.CreatedAfter);

            if (!string.IsNullOrEmpty(options.CityName))
                source = source.Where(x => EF.Functions.ILike(x.CityName, $"%{options.CityName}%"));
            if (!string.IsNullOrEmpty(options.CountryName))
                source = source.Where(x => EF.Functions.ILike(x.CountryName, $"%{options.CountryName}%"));
            if (!string.IsNullOrEmpty(options.Distance))
                source = source.Where(x => EF.Functions.ILike(x.Distance, $"%{options.Distance}%"));
            if (!string.IsNullOrEmpty(options.Email))
                source = source.Where(x => EF.Functions.ILike(x.Email, $"%{options.Email}%"));
            if (!string.IsNullOrEmpty(options.ExtraContact))
                source = source.Where(x => EF.Functions.ILike(x.ExtraContact, $"%{options.ExtraContact}%"));
            if (!string.IsNullOrEmpty(options.FirstName))
                source = source.Where(x => EF.Functions.ILike(x.FirstName, $"%{options.FirstName}%"));
            if (!string.IsNullOrEmpty(options.GenderName))
                source = source.Where(x => EF.Functions.ILike(x.GenderName, $"%{options.GenderName}%"));
            if (!string.IsNullOrEmpty(options.LastName))
                source = source.Where(x => EF.Functions.ILike(x.LastName, $"%{options.LastName}%"));
            if (!string.IsNullOrEmpty(options.PhoneNumber))
                source = source.Where(x => EF.Functions.ILike(x.PhoneNumber, $"%{options.PhoneNumber}%"));
            if (!string.IsNullOrEmpty(options.Size))
                source = source.Where(x => EF.Functions.ILike(x.Size, $"%{options.Size}%"));
            if (options.marathonEnum.HasFlag(MarathonEnum.ForestTrail))
                source = source.Where(x => x.marathonEnum == MarathonEnum.ForestTrail);
            if (options.marathonEnum.HasFlag(MarathonEnum.Ishim))
                source = source.Where(x => x.marathonEnum == MarathonEnum.Ishim);

            return source;
        }

        public static IQueryable<MarathonUsers> ApplySorting(this IQueryable<MarathonUsers> source, MarathonUsersSortState sortState)
        {
            return sortState switch
            {
                MarathonUsersSortState.IdAsc => source.OrderBy(x => x.Id),
                MarathonUsersSortState.IdDesc => source.OrderByDescending(x => x.Id),
                MarathonUsersSortState.EmailAsc => source.OrderBy(x => x.Email),
                MarathonUsersSortState.EmailDesc => source.OrderByDescending(x => x.Email),
                MarathonUsersSortState.PhoneNumberAsc => source.OrderBy(x => x.PhoneNumber),
                MarathonUsersSortState.PhoneNumberDesc => source.OrderByDescending(x => x.PhoneNumber),
                MarathonUsersSortState.FirstNameAsc => source.OrderBy(x => x.FirstName),
                MarathonUsersSortState.FirstNameDesc => source.OrderByDescending(x => x.FirstName),
                MarathonUsersSortState.LastNameAsc => source.OrderBy(x => x.LastName),
                MarathonUsersSortState.LastNameDesc => source.OrderByDescending(x => x.LastName),
                MarathonUsersSortState.GenderNameAsc => source.OrderBy(x => x.GenderName),
                MarathonUsersSortState.GenderNameDesc => source.OrderByDescending(x => x.GenderName),
                MarathonUsersSortState.CountryNameAsc => source.OrderBy(x => x.CountryName),
                MarathonUsersSortState.CountryNameDesc => source.OrderByDescending(x => x.CountryName),
                MarathonUsersSortState.CityNameAsc => source.OrderBy(x => x.CityName),
                MarathonUsersSortState.CityNameDesc => source.OrderByDescending(x => x.CityName),
                MarathonUsersSortState.MarathonEnumAsc => source.OrderBy(x => x.marathonEnum),
                MarathonUsersSortState.MarathonEnumDesc => source.OrderByDescending(x => x.marathonEnum),
                _ => source,
            };
        }

        public enum MarathonUsersSortState
        {
            IdAsc, CreatedDateAsc, EmailAsc, PhoneNumberAsc, FirstNameAsc, LastNameAsc, GenderNameAsc, CountryNameAsc, CityNameAsc, MarathonEnumAsc,
            IdDesc, CreatedDateDesc, EmailDesc, PhoneNumberDesc, FirstNameDesc, LastNameDesc, GenderNameDesc, CountryNameDesc, CityNameDesc, MarathonEnumDesc
        }
    }
}