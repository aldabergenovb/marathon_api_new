using System.Linq;
using Microsoft.EntityFrameworkCore;
using pmo_api.Models.Base;
using pmo_api.Models.Filters;

namespace pmo_api.Helpers
{
    public static class UsersHelper
    {
        public static IQueryable<AppUser> ApplyFilters(this IQueryable<AppUser> source, UserFilters options)
        {
            if (!string.IsNullOrEmpty(options.FullName))
                source = source.Where(x => EF.Functions.ILike(x.LastName + " " + x.FirstName + " " + x.MiddleName, $"%{options.FullName}%"));

            if (!string.IsNullOrEmpty(options.FirstName))
                source = source.Where(x => EF.Functions.ILike(x.FirstName, $"%{options.FirstName}%"));

            if (!string.IsNullOrEmpty(options.LastName))
                source = source.Where(x => EF.Functions.ILike(x.LastName, $"%{options.LastName}%"));

            if (!string.IsNullOrEmpty(options.MiddleName))
                source = source.Where(x => EF.Functions.ILike(x.MiddleName, $"%{options.MiddleName}%"));

            if (!string.IsNullOrEmpty(options.PositionName))
                source = source.Where(x => EF.Functions.ILike(x.PositionName, $"%{options.PositionName}%"));

            if (!string.IsNullOrEmpty(options.PositionNameKz))
                source = source.Where(x => EF.Functions.ILike(x.PositionNameKz, $"%{options.PositionNameKz}%"));

            if (!string.IsNullOrEmpty(options.Email))
                source = source.Where(x => EF.Functions.ILike(x.Email, $"%{options.Email}%"));

            if (options.Role?.Any() == true)
                source = source.Where(x => options.Role.Contains(x.UserRoles.FirstOrDefault().Role.Name));

            if (options.StartCreated != null)
                source = source.Where(x => x.Created >= options.StartCreated);

            if (options.StartCreated != null)
                source = source.Where(x => x.Created >= options.StartCreated);
            return source;
        }

        public static IQueryable<AppUser> ApplySorting(this IQueryable<AppUser> source, AppUsersSortState sortState)
        {
            return sortState switch
            {
                AppUsersSortState.LastNameAsc => source.OrderBy(x => x.LastName),
                AppUsersSortState.LastNameDesc => source.OrderByDescending(x => x.LastName),
                AppUsersSortState.FirstNameAsc => source.OrderBy(x => x.FirstName),
                AppUsersSortState.FirstNameDesc => source.OrderByDescending(x => x.FirstName),
                AppUsersSortState.MiddleNameAsc => source.OrderBy(x => x.MiddleName),
                AppUsersSortState.MiddleNameDesc => source.OrderByDescending(x => x.MiddleName),
                AppUsersSortState.PositionNameAsc => source.OrderBy(x => x.PositionName),
                AppUsersSortState.PositionNameDesc => source.OrderByDescending(x => x.PositionName),
                AppUsersSortState.PositionNameKzAsc => source.OrderBy(x => x.PositionNameKz),
                AppUsersSortState.PositionNameKzDesc => source.OrderByDescending(x => x.PositionNameKz),
                AppUsersSortState.EmailAsc => source.OrderBy(x => x.Email),
                AppUsersSortState.EmailDesc => source.OrderByDescending(x => x.Email),
                AppUsersSortState.CreatedAsc => source.OrderBy(x => x.Created),
                AppUsersSortState.CreatedDesc => source.OrderByDescending(x => x.Created),
                _ => source,
            };
        }
    }

    public enum AppUsersSortState
    {
        LastNameAsc, FirstNameAsc, MiddleNameAsc, PositionNameAsc, PositionNameKzAsc, OrganizationNameAsc, EmailAsc, RoleAsc, CreatedAsc,
        LastNameDesc, FirstNameDesc, MiddleNameDesc, PositionNameDesc, PositionNameKzDesc, OrganizationNameDesc, EmailDesc, RoleDesc, CreatedDesc
    }
}