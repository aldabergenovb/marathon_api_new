using pmo_api.Constants;

namespace pmo_api.Helpers
{
    public static class MessageHelper
    {
        public static string SuccessSaved(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Kz => "Деректер сәтті сақталды",
                CultureConstants.Languages.Ru => "Данные успешно сохранены",
                _ => "Data saved successfully",
            };
        }

        public static string SuccessDeleted(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Kz => "Деректер сәтті жойылды",
                CultureConstants.Languages.Ru => "Данные успешно удалены",
                _ => "Data successfully deleted",
            };
        }

        public static string AccessDenied(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Kz => "Рұқсат жоқ",
                CultureConstants.Languages.Ru => "Нет доступа",
                _ => "No access",
            };
        }

        public static string InvalidData(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Kz => "Дұрыс емес деректер",
                CultureConstants.Languages.Ru => "Некорректные данные",
                _ => "Incorrect data",
            };
        }

        public static string ServerError(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Kz => "Қызмет қатесі",
                CultureConstants.Languages.Ru => "Ошибка сервиса",
                _ => "Service error",
            };
        }

        public static string EmptyRequiredFields(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Kz => "Барлық қажетті өрістерді толтырыңыз",
                CultureConstants.Languages.Ru => "Заполните все обязательные поля",
                _ => "Please fill in all required fields",
            };
        }

        public static string EmailIsRegistered(string language, string additionalText = "")
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Kz => $"Жіберілген пошта тіркелген {additionalText}",
                CultureConstants.Languages.Ru => $"Указанная почта уже зарегистрирована {additionalText}",
                _ => $"The specified mail is already registered {additionalText}",
            };
        }

        public static string AlreadyExists(string language, string additionalText = "")
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Kz => $"Осындай деректері бар объект бұрыннан бар: {additionalText}",
                CultureConstants.Languages.Ru => $"Объект с этими данными уже существует: {additionalText}",
                _ => $"{additionalText}",
            };
        }

        public static string AlreadyListExists(string language, string additionalText = "")
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Kz => $"Осындай деректері бар объектілер бұрыннан бар: {additionalText}",
                CultureConstants.Languages.Ru => $"Некоторые объекты с этими данными уже существуют: {additionalText}",
                _ => $"{additionalText}",
            };
        }

        public static string DataNotFound(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Kz => "Деректер табылмады",
                CultureConstants.Languages.Ru => "Данные не найдены",
                _ => "Data not found",
            };
        }

        public static string PasswordTooShort(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Пароль должен состоять не менее 8 символов",
                CultureConstants.Languages.Kz => "Құпия сөз кемінде 8 таңбадан тұруы керек",
                _ => "Password must be at least 8 characters",
            };
        }

        public static string PasswordRequiresNonAlphanumeric(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Пароль должен содержать хотя бы один спецсимвол",
                CultureConstants.Languages.Kz => "Құпия сөзде кемінде бір арнайы таңба болуы керек",
                _ => "Password must contain at least one special character",
            };
        }

        public static string PasswordRequiresDigit(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Пароль должен содержать хотя бы одну цифру",
                CultureConstants.Languages.Kz => "Құпия сөзде кемінде бір сан болуы керек",
                _ => "Password must contain at least one number",
            };
        }

        public static string PasswordRequiresLower(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Пароль должен содержать нижний регистр символов",
                CultureConstants.Languages.Kz => "Құпия сөзде кіші әріптер болуы керек",
                _ => "Password must contain lowercase characters",
            };
        }

        public static string PasswordRequiresUpper(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Пароль должен содержать верхний регистр символов",
                CultureConstants.Languages.Kz => "Құпия сөзде бас әріптер болуы керек",
                _ => "Password must contain uppercase characters",
            };
        }

        public static string PasswordIncorrectAuth(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Ошибка аутентификации. Неверный логин/пароль",
                CultureConstants.Languages.Kz => "Сәйкестендіру қатесі. Дұрыс емес логин/құпия сөз",
                _ => "Authentication failed. Incorrect login/password",
            };
        }

        public static string PasswordIncorrect(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Текущий пароль неверный",
                CultureConstants.Languages.Kz => "Ағымдағы құпия сөз дұрыс емес",
                _ => "The current password is incorrect",
            };
        }

        public static string PasswordNotMatch(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Пароли не совпадают",
                CultureConstants.Languages.Kz => "Құпия сөздер сәйкес келмейді",
                _ => "Passwords do not match",
            };
        }

        public static string PasswordChanged(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Пароль успешно изменен",
                CultureConstants.Languages.Kz => "Құпия сөз өзгертілді",
                _ => "Password changed",
            };
        }

        public static string UserNotFound(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Пользователь не найден",
                CultureConstants.Languages.Kz => "Пайдаланушы табылмады",
                _ => "User not found",
            };
        }

        public static string EmailNotConfirmed(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Электронная почта не подтверждена",
                CultureConstants.Languages.Kz => "Электрондық пошта расталмаған",
                _ => "Email not confirmed",
            };
        }

        public static string EmailIsConfirmed(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Электронная почта уже подтверждена",
                CultureConstants.Languages.Kz => "Электрондық пошта расталған",
                _ => "Email confirmed",
            };
        }

        public static string UserDeactivated(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Пользователь деактивирован",
                CultureConstants.Languages.Kz => "Пайдаланушы белсендірілмеген",
                _ => "User deactivated",
            };
        }

        public static string UserAccessSuccess(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Учетная запись подтверждена",
                CultureConstants.Languages.Kz => "Тіркелгі расталды",
                _ => "Account Confirmed",
            };
        }

        public static string ActivationTermExpired(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Срок активации истек",
                CultureConstants.Languages.Kz => "Белсендіру мерзімі аяқталды",
                _ => "Activation period has expired",
            };
        }

        public static string EmailConfirmMessage(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Ссылка на восстановление отправлена на электронную почту",
                CultureConstants.Languages.Kz => "Қалпына келтіру сілтемесі электрондық поштаға жіберілді",
                _ => "Recovery link sent to e-mail",
            };
        }

        public static string ForgotPasswordLink(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Ссылка на восстановление уже использована, либо срок восстановления пароля истек. Выполните запрос еще раз.",
                CultureConstants.Languages.Kz => "Қалпына келтіру сілтемесі пайдаланылды немесе құпия сөзді қалпына келтіру мерзімі аяқталды. Сұрауды қайта орындаңыз.",
                _ => "The recovery link has already been used or your password has expired. Run the query again.",
            };
        }

        public static string PhoneNumberExist(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Указанный номер телефона уже существует",
                CultureConstants.Languages.Kz => "Көрсетілген телефон нөмірі бұрыннан бар",
                _ => "The specified phone number already exists",
            };
        }

        public static string ProfileChanged(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Профиль успешно изменен",
                CultureConstants.Languages.Kz => "Профиль сәтті өзгертілді",
                _ => "Profile changed successfully",
            };
        }

        public static string ProfileSaveError(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Ошибка изменения данных пользователя",
                CultureConstants.Languages.Kz => "Пайдаланушы деректерін өзгерту қатесі",
                _ => "Error modifying user data",
            };
        }

        public static string ProfilePhotoNotFound(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Фото профиля не найдено",
                CultureConstants.Languages.Kz => "Профайлдың суреті табылмады",
                _ => "Profile photo not found",
            };
        }

        public static string ProfilePhotoDeleted(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Фото профиля успешно удалено",
                CultureConstants.Languages.Kz => "Профайлдың суреті сәтті жойылды",
                _ => "Profile photo successfully deleted",
            };
        }

        public static string EmailIsExist(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Пользователь с такой электронной почтой уже существует",
                CultureConstants.Languages.Kz => "Мұндай электрондық пошта бар пайдаланушы бұрыннан бар",
                _ => "A user with this e-mail already exists",
            };
        }

        public static string RegisterUserError(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Ошибка создания пользователя",
                CultureConstants.Languages.Kz => "Пайдаланушыны жасау қатесі",
                _ => "Error creating user",
            };
        }

        public static string RegisterSuccess(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Ваша заявка принято в работу. Ожидайте ответа",
                CultureConstants.Languages.Kz => "Сіздің өтініміңіз жұмысқа қабылданды. Жауап күтіңіз",
                _ => "Your application has been accepted. Expect a response",
            };
        }

        public static string InputError(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Ошибка входных данных",
                CultureConstants.Languages.Kz => "Кіріс деректерінің қатесі",
                _ => "Input error",
            };
        }

        public static string RoleIsNotExist(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Роль не существует",
                CultureConstants.Languages.Kz => "Роль не существует",
                _ => "Роль не существует",
            };
        }

        public static string EmployeeAtached(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Сотрудник прикреплен",
                CultureConstants.Languages.Kz => "Қызметкер бекітілген",
                _ => "Employee attached",
            };
        }
        public static string EmployeeDetached(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Сотрудник откреплен",
                CultureConstants.Languages.Kz => "Қызметкер бекітілді",
                _ => "Employee detached",
            };
        }

        public static string ContactAlreadyExist(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Сотрудник уже существует в этой организации",
                CultureConstants.Languages.Kz => "Қызметкер осы ұйымда бұрыннан бар",
                _ => "An employee already exists in this organization",
            };
        }

        public static string MessageSendSuccess(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Сообщение успешно отправлено",
                CultureConstants.Languages.Kz => "Хабар сәтті жіберілді",
                _ => "Message sent successfully",
            };
        }

        public static string FileSizeError(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => $"Размер файла не должен превышать {CommonConstants.MaxFileSizeInMegaBytes} МБ",
                CultureConstants.Languages.Kz => $"Файл өлшемі  {CommonConstants.MaxFileSizeInMegaBytes} мб аспауы тиіс",
                _ => $"File size must not exceed {CommonConstants.MaxFileSizeInMegaBytes} MB",
            };
        }

        public static string FileCountError(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => $"Количество файлов не должно превышать {CommonConstants.MaxUploadFilesCount} шт.",
                CultureConstants.Languages.Kz => $"Файлдар саны {CommonConstants.MaxUploadFilesCount} данадан аспауы тиіс.",
                _ => $"The number of files must not exceed {CommonConstants.MaxUploadFilesCount} pc.",
            };
        }

        public static string FileIsNotExist(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Файл не был получен",
                CultureConstants.Languages.Kz => "Файл алынбады",
                _ => "The file was not received",
            };
        }

        public static string RestoreSuccess(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Данные успешно восстановлен",
                CultureConstants.Languages.Kz => "Деректер сәтті қалпына келтірілді",
                _ => "Данные успешно восстановлен",
            };
        }

        public static string RecordSuccessDuplicate(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Заявка успешно дублирована",
                CultureConstants.Languages.Kz => "Өтінім сәтті қайталанды",
                _ => "Request successfully duplicated",
            };
        }

        public static string ProjectStageTypeRealization(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Стадия реализации",
                CultureConstants.Languages.Kz => "Іске асыру сатысы",
                _ => "Sales phase",
            };
        }

        public static string ProjectStageTypePreparation(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Подготовка и конкурс",
                CultureConstants.Languages.Kz => "Дайындық және конкурс",
                _ => "Preparation and competition",
            };
        }

        public static string FindTextError(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Минимальное количество символов для поиска равно 3",
                CultureConstants.Languages.Kz => "Іздеу үшін таңбалардың ең аз саны 3-ке тең",
                _ => "The minimum number of characters to search is 3",
            };
        }

        public static string DictionaryNotFound(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Справочник не найден",
                CultureConstants.Languages.Kz => "Анықтама табылмады",
                _ => "Directory not found",
            };
        }

        public static string HeadChooseError(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Нельзя открепить руководителя организации, выберите нового руководителя",
                CultureConstants.Languages.Kz => "Ұйым басшысын есептен шығаруға болмайды, жаңа басшыны таңдаңыз",
                _ => "You cannot detach an organization manager, select a new manager",
            };
        }

        public static string DeleteProjectRelation(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Вы не можете удалить этот элемент так как он связан с проектом, выберите другой элемент в проекте",
                CultureConstants.Languages.Kz => "Жобамен байланысты болғандықтан сіз бұл элементті жоя алмайсыз, жобада басқа элементті таңдаңыз",
                _ => "You cannot delete this item because it is associated with the project, select another item in the project",
            };
        }

        public static string DeleteProjectRelationList(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Вы не можете удалить некоторые элементы так как они связаны с проектами, выберите другой элемент в проектах",
                CultureConstants.Languages.Kz => "Жобалармен байланысты болғандықтан cіз кейбір элементтерді жоя алмайсыз , жобаларда басқа элементті таңдаңыз",
                _ => "You cannot delete some items because they are associated with projects, select another item in projects",
            };
        }

        public static string IssueTasksNotCompleted(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Задачи не завершены",
                CultureConstants.Languages.Kz => "Тапсырмалар аяқталмады",
                _ => "Tasks are not completed",
            };
        }

        public static string IssueTasksNotCreated(string language)
        {
            return language.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "У поручения нет задач",
                CultureConstants.Languages.Kz => "Тапсырмалар жоқ",
                _ => "The assignment has no tasks",
            };
        }
    }
}