using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using pmo_api.Models.Enums;
using pmo_api.Models.Base;
using pmo_api.Models.Emails;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using pmo_api.Constants;
using pmo_api.Models.Directories;
using System.Threading.Tasks;

namespace pmo_api.Helpers
{
    public static class AccountHelper
    {
        public static async Task<AppUserRoleVM> CheckUserClaims(UserManager<AppUser> userManager, ClaimsPrincipal userClaims, string lang)
        {
            var user = await userManager.GetUserAsync(userClaims);
            if (user is null)
                return null;

            var userRole = await userManager.GetRolesAsync(user);
            if (userRole is null || userRole.Count == 0 || userRole[0] is null)
                return null;

            var newObject = new AppUserRoleVM() { User = user, Role = userRole[0] };

            return newObject;
        }

        public static object GenerateJwtToken(IConfiguration _configuration, AppUser user, string role, string refToken, string organizationId, bool isHead = false, bool isSavedPass = false)
        {
            if (string.IsNullOrWhiteSpace(organizationId))
            {
                organizationId = string.Empty;
                isHead = false;
            }

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Role, role), // для авторизации по ролям токен должен содержать именно такой claim. ClaimTypes.Role
                new Claim("role", role),
                new Claim("userId", user.Id.ToString()),
                new Claim("fullname", user.FullName),
                new Claim("refresh_token", refToken),
                new Claim("organizationId", organizationId),
                new Claim("isHead", isHead.ToString().ToLower())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var expires = GetJwtExpiresDateTimeByRole(_configuration, TimeUnits.Hours, isSavedPass);

            var token = new JwtSecurityToken(
                _configuration["JwtIssuer"],
                _configuration["JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public static DateTime GetJwtExpiresDateTimeByRole(IConfiguration _configuration, TimeUnits timeUnit = TimeUnits.Hours, bool isSavedPass = false)
        {
            var keyPath = GetJwtExpireConfigKeyPath(timeUnit);
            var expireDateTimeOffset = DateTimeOffset.UtcNow;

            if (timeUnit == TimeUnits.Hours)
            {
                var expireTimeUnits = Convert.ToDouble(_configuration[keyPath]);
                if (isSavedPass)
                    expireDateTimeOffset = expireDateTimeOffset.AddYears(1);
                else
                    expireDateTimeOffset = expireDateTimeOffset.AddHours(expireTimeUnits);
            }

            return expireDateTimeOffset.DateTime;
        }

        private static string GetJwtExpireConfigKeyPath(TimeUnits timeUnit)
        {
            const string target = "all";

            // if (role == UserConstants.Roles.Visitor)
            //     target = UserConstants.Roles.Visitor;

            var timeUnitName = timeUnit.ToString();
            return $"JwtExpires:{target}:{timeUnitName}";
        }

        /// <summary>
        /// Generates a Random Password
        /// respecting the given strength requirements.
        /// </summary>
        /// <param name="opts">A valid PasswordOptions object
        /// containing the password strength requirements.</param>
        /// <returns>A random password</returns>
        public static string GenerateRandomPassword(PasswordOptions opts = null)
        {
            opts ??= GetPasswordOptions();

            string[] randomChars = new[] {
                "ABCDEFGHJKLMNOPQRSTUVWXYZ",    // uppercase 
                "abcdefghijkmnopqrstuvwxyz",    // lowercase
                "0123456789",                   // digits
                "!@$?_-"                        // non-alphanumeric
            };
            Random rand = new(Environment.TickCount);
            List<char> chars = new();

            if (opts.RequireUppercase)
                chars.Insert(rand.Next(0, chars.Count), randomChars[0][rand.Next(0, randomChars[0].Length)]);

            if (opts.RequireLowercase)
                chars.Insert(rand.Next(0, chars.Count), randomChars[1][rand.Next(0, randomChars[1].Length)]);

            if (opts.RequireDigit)
                chars.Insert(rand.Next(0, chars.Count), randomChars[2][rand.Next(0, randomChars[2].Length)]);

            if (opts.RequireNonAlphanumeric)
                chars.Insert(rand.Next(0, chars.Count), randomChars[3][rand.Next(0, randomChars[3].Length)]);

            for (int i = chars.Count; i < opts.RequiredLength || chars.Distinct().Count() < opts.RequiredUniqueChars; i++)
            {
                string rcs = randomChars[rand.Next(0, randomChars.Length)];
                chars.Insert(rand.Next(0, chars.Count), rcs[rand.Next(0, rcs.Length)]);
            }

            return new string(chars.ToArray());
        }

        public static PasswordOptions GetPasswordOptions() => new()
        {
            RequiredLength = 8,
            RequiredUniqueChars = 1,
            RequireDigit = true,
            RequireLowercase = true,
            RequireNonAlphanumeric = true,
            RequireUppercase = true
        };

        public static string GetChangePasswordErrorMessage(IEnumerable<IdentityError> errors)
        {
            string errorMessage = "Пароль должен содержать:";
            foreach (var error in errors)
            {
                switch (error.Code)
                {
                    case "PasswordTooShort":
                        errorMessage += " не менее 8 символов,";
                        break;
                    case "PasswordRequiresNonAlphanumeric":
                        errorMessage += " специальный символ,";
                        break;
                    case "PasswordRequiresLower":
                        errorMessage += " строчный символ,";
                        break;
                    case "PasswordRequiresUpper":
                        errorMessage += " прописной символ,";
                        break;
                    case "PasswordRequiresDigit":
                        errorMessage += " цифру,";
                        break;
                    default: break;
                }
            }
            return errorMessage.TrimEnd(',');
        }

        public static OutgoingEmail GetForgotPasswordMessage(AppUser user, string callbackUrl, string lang)
        {
            string subject = lang.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Восстановление пароля в информационной системе PMO",
                CultureConstants.Languages.Kz => "PMO ақпараттық жүйесінде құпия сөзді қалпына келтіру",
                _ => "testEn"
            };

            var message = lang.ToUpper() switch
            {
                CultureConstants.Languages.Ru => $@"Здравствуйте, {user.FullName}!<br/>
                    <br/>
                    Для восстановления пароля в информационной системе PMO перейдите по <a href='{callbackUrl}'>ссылке</a> или скопируйте её в адресную строку браузера.<br/>
                    <br/>
                    Это одноразовая ссылка для входа перенаправит Вас на страницу, где Вы сможете изменить пароль. Ссылка действительна в течение 24 часов. Если она не будет использована, необходимо заново пройти процедуру восстановления пароля.
                    <br/>
                    <br/>
                    С уважением, Информационная система PMO
                    <br/>
                    <br/>
                    Сообщение было отправлено автоматически. Пожалуйста, не отвечайте на это сообщение.",
                CultureConstants.Languages.Kz => $@"Сәлеметсіз бе, {user.FullName}!<br/>
                    <br/>
                    PMO ақпараттық жүйесіндегі құпия сөзді қалпына келтіру үшін <a href='{callbackUrl}'>сілтемеге</a> өтіңіз немесе оны браузердің мекенжай жолағына көшіріңіз.<br/>
                    <br/>
                    Бұл бір реттік кіру сілтемесі Сізді құпия сөзді өзгертуге болатын бетке бағыттайды. Сілтеме 24 сағат ішінде жарамды. Егер ол қолданылмаса, құпия сөзді қалпына келтіру процедурасынан өту керек.
                    <br/>
                    <br/>
                    Құрметпен, PMO ақпараттық жүйесі 
                    <br/>
                    <br/>
                    Хабарлама автоматты түрде жіберілді. Өтінеміз, бұл хабарламаға жауап бермеңіз.",
                _ => "testEn"
            };
            return new OutgoingEmail(user.Email, subject, message);
        }

        public static OutgoingEmail RegistrationAlert(string userFullName, string login, string password, string callbackUrl, string email, string lang)
        {
            string subject = lang.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Регистрация в информационной системе PMO",
                CultureConstants.Languages.Kz => "PMO ақпараттық жүйесінде тіркелу",
                _ => "testEn"
            };
            var message = lang.ToUpper() switch
            {
                CultureConstants.Languages.Ru => $@"Здравствуйте, {userFullName}!<br/>
                    <br/>
                    Чтобы активировать Ваш аккаунт в информационной системе PMO, пожалуйста, используйте <a href='{callbackUrl}'>ссылку</a>
                    <br/>
                    Логин: {login}
                    <br/>
                    Пароль: {password}
                    <br/>
                    <br/>
                    Внимание!<br/>
                    Активация аккаунта производится только по данной ссылке.<br/>
                    Отвечать на данное сообщение не нужно.<br/>
                    Ссылка действительна в течение 24 часов.<br/>
                    <br/>
                    Вы получили это сообщение потому, что Ваш e-mail адрес был зарегистрирован в информационной системе PMO. Если Вы не регистрировались в этой информационной системе, пожалуйста, проигнорируйте это письмо.
                    <br/>
                    <br/>
                    С уважением, Информационная система PMO
                    <br/>
                    <br/>
                    Сообщение было отправлено автоматически. Пожалуйста, не отвечайте на это сообщение.",
                CultureConstants.Languages.Kz => $@"Сәлеметсіз бе, {userFullName}!<br/>
                    <br/>
                    Сіздің PMO ақпараттық жүйесінде тіркелгіңізді белсендіру үшін <a href='{callbackUrl}'>сілтемені</a> пайдаланыңыз.
                    <br/>
                    Логин: {login}
                    <br/>
                    Құпия сөз: {password}
                    <br/>
                    <br/>
                    Назар аударыңыз!<br/>
                    Есептік жазбаны белсендіру тек осы сілтеме арқылы жүзеге асырылады.<br/>
                    Бұл хабарламаға жауап берудің қажеті жоқ.<br/>
                    Сілтеме 24 сағат ішінде жарамды.<br/>
                    <br/>
                    Сіз бұл хабарламаны Сіздің e-mail мекенжайыңыз PMO ақпараттық жүйесінде тіркелгендіктен алдыңыз. Егер Сіз осы ақпараттық жүйесінде тіркелмеген болсаңыз, бұл хатты елемеңіз.
                    <br/>
                    <br/>
                    Құрметпен, PMO ақпараттық жүйесі 
                    <br/>
                    <br/>
                    Хабарлама автоматты түрде жіберілді. Өтінеміз, бұл хабарламаға жауап бермеңіз.",
                _ => "testEn"
            };

            return new OutgoingEmail(email, subject, message);
        }

        public static OutgoingEmail ResendLetterAlert(string userFullName, string login, string password, string callbackUrl, string email, string lang)
        {
            string subject = lang.ToUpper() switch
            {
                CultureConstants.Languages.Ru => "Регистрация в информационной системе PMO",
                CultureConstants.Languages.Kz => "PMO ақпараттық жүйесінде тіркелу",
                _ => "testEn"
            };
            var message = lang.ToUpper() switch
            {
                CultureConstants.Languages.Ru => $@"Здравствуйте, {userFullName}!<br/>
                    <br/>
                    Чтобы активировать Ваш аккаунт в информационной системе PMO, пожалуйста, используйте <a href='{callbackUrl}'>ссылку</a>
                    <br/>
                    Логин: {login}
                    <br/>
                    Пароль: {password}
                    <br/>
                    <br/>
                    Внимание!<br/>
                    Активация аккаунта производится только по данной ссылке.<br/>
                    Отвечать на данное сообщение не нужно.<br/>
                    Ссылка действительна в течение 24 часов.<br/>
                    <br/>
                    Письмо было перенаправлено с новыми данными, так как срок ранее отправленного письма истек.
                    <br/>
                    Вы получили это сообщение потому, что Ваш e-mail адрес был зарегистрирован в информационной системе PMO. Если Вы не регистрировались в этой информационной системе, пожалуйста, проигнорируйте это письмо.
                    <br/>
                    <br/>
                    С уважением, Информационная система PMO
                    <br/>
                    <br/>
                    Сообщение было отправлено автоматически. Пожалуйста, не отвечайте на это сообщение.",
                CultureConstants.Languages.Kz => $@"Сәлеметсіз бе, {userFullName}!<br/>
                    <br/>
                    Сіздің PMO ақпараттық жүйесінде тіркелгіңізді белсендіру үшін <a href='{callbackUrl}'>сілтемені</a> пайдаланыңыз.
                    <br/>
                    Логин: {login}
                    <br/>
                    Құпия сөз: {password}
                    <br/>
                    <br/>
                    Назар аударыңыз!<br/>
                    Есептік жазбаны белсендіру тек осы сілтеме арқылы жүзеге асырылады.<br/>
                    Бұл хабарламаға жауап берудің қажеті жоқ.<br/>
                    Сілтеме 24 сағат ішінде жарамды.<br/>
                    <br/>
                    Хат жаңа мәліметтермен қайта жіберілді, өйткені бұрын жіберілген хаттың мерзімі аяқталды.
                    <br/>
                    Сіз бұл хабарламаны Сіздің e-mail мекенжайыңыз PMO ақпараттық жүйесінде тіркелгендіктен алдыңыз. Егер Сіз осы сайтқа тіркелмеген болсаңыз, бұл хатты елемеңіз.
                    <br/>
                    <br/>
                    Құрметпен, PMO ақпараттық жүйесі 
                    <br/>
                    <br/>
                    Хабарлама автоматты түрде жіберілді. Өтінеміз, бұл хабарламаға жауап бермеңіз.",
                _ => "testEn"
            };
            return new OutgoingEmail(email, subject, message);
        }
    }
}