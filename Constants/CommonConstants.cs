namespace pmo_api.Constants
{
    public static class CommonConstants
    {
        public const long MaxFileSizeInMegaBytes = 10;
        public const long MaxFileSizeInBytes = MaxFileSizeInMegaBytes * 1048576; // 10 MB
        public const int MaxUploadFilesCount = 5;

        public static class ModalFieldCodes
        {
            public const string Reason = "REASON";
            public const string DebtSum = "DEBT_SUM";
            public const string DeclarationNumber = "DECLARATION_NUMBER";
            public const string ApplicationNumber = "APPLICATION_NUMBER";
            public const string CustomsOverpayment = "CUSTOMS_OVERPAYMENT";
            public const string TotalCostOfGoods = "TOTAL_COST_OF_GOODS";
            public const string File = "FILE";
            public const string SalesManagers = "SALES_MANAGERS";
            public const string LogisticsManagers = "LOGISTICS_MANAGERS";
            public const string SeniorSalesManagers = "SENIOR_SALES_MANAGERS";
            public const string AllSalesManagers = "ALL_SALES_MANAGERS";
            public const string AllEmployees = "ALL_EMPLOYEES";
            public const string ContractTypes = "CONTRACT_TYPES";
            public const string AllKtoManagers = "ALL_KTO_MANAGERS";
            public const string Contracts = "CONTRACTS";
            public const string AllManagers = "ALL_MANAGERS";
            public const string AllOOKManagers = "ALL_OOK_MANAGERS";
            public const string AllOTUManagers = "ALL_OTU_MANAGERS";
            public const string AllOBUManagers = "ALL_OBU_MANAGERS";
            public const string AllOTPManagers = "ALL_OTP_MANAGERS";
        }
    }
}