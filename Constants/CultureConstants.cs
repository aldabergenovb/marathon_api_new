namespace pmo_api.Constants
{
    public static class CultureConstants
    {
        public static class Languages
        {
            public const string Kz = "KZ";
            public const string En = "EN";
            public const string Ru = "RU";
        }

        public const string RestAllCountriesUri = "https://restcountries.eu/rest/v2/all?fields=name";
    }
}