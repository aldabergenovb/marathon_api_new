using System.Collections.Generic;

namespace pmo_api.Constants
{
    public static class UserConstants
    {
        public static class Roles
        {
            /// <summary>Системный пользователь </summary>
            public const string System = "system";
            /// <summary>Суперпользователь</summary>
            public const string Root = "root";
            /// <summary>Администратор системы</summary>
            public const string Admin = "admin";
            /// <summary>Менеджер проектов</summary>
            public const string Manager = "manager";
            /// <summary> Менеджер аппарата </summary>
            public const string OfficeManager = "officemanager";
            /// <summary> Руководитель куратора </summary>
            public const string OfficeCurator = "officecurator";
            /// <summary> Аким района </summary>
            public const string DistrictCurator = "districtcurator";
            /// <summary>Куратор (аким, зам акима)</summary>
            public const string Curator = "curator";
            /// <summary>Куратор (аким, зам акима)</summary>
            public const string HeadCurator = "head-curator";
            /// <summary>Координатор</summary>
            public const string Coordinator = "coordinator";
            /// <summary>Главный координатор (советник акима - Айгуль)</summary>
            public const string MainCoordinator = "main-coordinator";
        }

        public static IReadOnlyList<string> AllRoles { get; } = new string[]
        {
            Roles.System,
            Roles.Root,
            Roles.Admin,
            Roles.Manager,
            Roles.Curator,
            Roles.HeadCurator,
            Roles.DistrictCurator,
            Roles.OfficeManager,
            Roles.Coordinator,
            Roles.MainCoordinator
        };

        public static IReadOnlyList<string> EmployeeRoles { get; } = new string[]
        {
            Roles.Admin,
            Roles.Curator,
            Roles.Manager,
            Roles.HeadCurator,
            Roles.HeadCurator,
            Roles.DistrictCurator,
            Roles.OfficeManager,
            Roles.Coordinator,
            Roles.MainCoordinator
        };

        public static IReadOnlyList<string> UserManagerPasswordErrorCodes { get; } = new string[]
        {
            "PasswordTooShort",
            "PasswordRequiresNonAlphanumeric",
            "PasswordRequiresDigit",
            "PasswordRequiresLower",
            "PasswordRequiresUpper",
        };
    }
}