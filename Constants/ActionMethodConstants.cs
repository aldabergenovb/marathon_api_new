namespace pmo_api.Constants
{
    public static class ActionMethodConstants
    {
        public static class Results
        {
            public const string Success = "SUCCESS";
            public const string Error = "ERROR";
            public const string NotFound = "NOT_FOUND";
            public const string AccessDenied = "ACCESS_DENIED";
            public const string ServerError = "SERVER_ERROR";
            public const string InvalidData = "INVALID_DATA";
            public const string RecordIsExists = "RECORD_IS_EXISTS";
        }
    }
}
