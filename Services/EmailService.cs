using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using pmo_api.Models.MarathonUserss;

namespace marathon_api_new.Services
{
    public class EmailService
    {
        private readonly string _smtpServer;
        private readonly int _smtpPort;
        private readonly string _smtpUsername;
        private readonly string _smtpPassword;

        public EmailService(string smtpServer, int smtpPort, string smtpUsername, string smtpPassword)
        {
            _smtpServer = smtpServer;
            _smtpPort = smtpPort;
            _smtpUsername = smtpUsername;
            _smtpPassword = smtpPassword;
        }

        public void SendEmail(MarathonUsersCreateViewModel model, string subject, string body)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Aqmola-Marathon", _smtpUsername));
            message.To.Add(new MailboxAddress("", model.Email));
            message.Subject = subject;

            var bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = "<!DOCTYPE html>" +
                                        "<html lang=\"ru\">" +
                                        "<head>" +
                                        "<meta charset=\"UTF-8\">" +
                                        "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">" +
                                        "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">" +
                                        "<title>Успешная регистрация на марафон FOREST TRAIL</title>" +
                                        "</head>" +
                                        "<body style=\"font-family: Arial, sans-serif; background-color: #f2f2f2; margin: 0; padding: 0;\">" +
                                        "<div style=\"background-color: #007f5f; color: #ffffff; text-align: center; padding: 20px;\">" +
                                        "<h1>Марафон FOREST TRAIL</h1>" +
                                        "</div>" +
                                        "<div style=\"padding: 20px;\">" +
                                        "<h2>Поздравляем!</h2>" +
                                        "<p>Вы успешно зарегистрировались на марафон FOREST TRAIL, который состоится 22 октября 2023 г. в Национальном парке «Бурабай».</p>" +
                                        "<h3>Детали вашей регистрации:</h3>" +
                                        "<ul>" +
                                        $"<li><strong>Имя:</strong> {model.FirstName}</li>" +
                                        $"<li><strong>Фамилия:</strong> {model.LastName}</li>" +
                                        $"<li><strong>Дата рождения:</strong> {model.BirthDate}</li>" +
                                        $"<li><strong>Электронная почта:</strong> {model.Email}</li>" +
                                        $"<li><strong>Телефон:</strong> {model.PhoneNumber}</li>" +
                                        $"<li><strong>Выбранный маршрут:</strong> {model.Distance}</li>" +
                                        "</ul>" +
                                        "<h3>Информация о выдаче стартового набора:</h3>" +
                                        "<p>Для участия в марафоне необходимо получить стартовый набор. Чтобы получить стартовый набор необходимо предьявить удостоверение личности и чек об оплате (kaspi). Выдача стартовых наборов будет проходить следующим образом:</p>" +
                                        "<ul>" +
                                        "<li><strong>20 октября 2023 года:</strong> с 10:00 до 20:00</li>" +
                                        "<li><strong>21 октября 2023 года:</strong> с 10:00 до 20:00</li>" +
                                        "</ul>" +
                                        "<p>Места выдачи стартовых наборов:</p>" +
                                        "<ul>" +
                                        "<li><strong>20.10.2023 г.:</strong> Hotel «G EMPIRE» г. Астана, проспект Абая 63 (1 этаж)</li>" +
                                        "<li><strong>21.10.2023 г.:</strong> Курорт Боровое, зона старт/финиша</li>" +
                                        "</ul>" +
                                        "<h3>Содержание стартового набора:</h3>" +
                                        "<ul>" +
                                        "<li>Стартовый номер</li>" +
                                        "<li>Чип для фиксации результата</li>" +
                                        "<li>Сумка</li>" +
                                        "<li>Булавки</li>" +
                                        "<li>Сувенир</li>" +
                                        "</ul>" +
                                        "<p>Мы ценим ваш интерес к марафону и ждем вас на старте, чтобы вместе преодолеть увлекательные трассы и насладиться природой Национального парка «Бурабай».</p>" +
                                        "<p>Следите за нашими обновлениями на сайте и в социальных сетях, чтобы быть в курсе всех подробностей мероприятия.</p>" +
                                        "<p>Если у вас возникнут какие-либо вопросы, не стесняйтесь обращаться к нам по контактным данным, указанным ниже.</p>" +
                                        "<p>С наилучшими пожеланиями,<br>Организационный комитет «AQMOLA MARATHON»</p>" +
                                        "</div>" +
                                        "<div style=\"background-color: #007f5f; color: #ffffff; text-align: center; padding: 10px; margin-top: 20px;\">" +
                                        "<p>Следите за нами: <a href=\"https://instagram.com/aqmolamarathon2023?igshid=MzRlODBiNWFlZA==\" style=\"color: #ffffff; text-decoration: underline;\">Instagram</a></p>" +
                                        "</div>" +
                                        "</body>" +
                                        "</html>";

            message.Body = bodyBuilder.ToMessageBody();

            using var client = new SmtpClient();
            client.Connect(_smtpServer, _smtpPort, SecureSocketOptions.StartTls);
            client.Authenticate(_smtpUsername, _smtpPassword);
            client.Send(message);
            client.Disconnect(true);
        }
    }
}