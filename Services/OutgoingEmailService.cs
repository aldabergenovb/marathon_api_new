using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using pmo_api.Models.Directories;
using pmo_api.Models.Emails;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace pmo_api.Services
{
    public interface IOutgoingEmailService
    {
        Task<ActionMethodResult> SendNoReplyMailAsync(OutgoingEmail model);
        Task SendMailAsync(OutgoingEmail model);
        // Task<TResponse> SendHttpRequest<TResponse>(string _api, HttpMethod _httpMethod, string _token = null, object _requestData = null) where TResponse : class;
    }

    public class OutgoingEmailService : IOutgoingEmailService
    {
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;
        public OutgoingEmailService(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _configuration = configuration;
        }

        public async Task<ActionMethodResult> SendNoReplyMailAsync(OutgoingEmail model)
        {
            Console.WriteLine($"Sending a no reply letter to the {model.Email} email...");

            var outgoingEmailForm = new OutgoingEmailForm(model);
            outgoingEmailForm.SetNoReplyConfig(_configuration);

            _httpClient.BaseAddress = new Uri(_configuration.GetValue<string>("OutgoingEmailService:host"));
            const string endpoint = "/api/email/send-email";
            var result = await SendHttpRequest<object>(endpoint, HttpMethod.Post, null, outgoingEmailForm);
            if (result != null)
            {
                Console.WriteLine($"A letter to the {model.Email} email sent successfully!");
                Console.WriteLine("The process of sending the letter has been completed.");
                return ActionMethodResult.Success();
            }
            else
            {
                Console.WriteLine($"Error of a letter sending to the {model.Email} email!!!");
                Console.WriteLine("The process of sending the letter has been completed.");
                return ActionMethodResult.Error("Ошибка интеграционного сервиса");
            }
        }

        public async Task SendMailAsync(OutgoingEmail model)
        {
            Console.WriteLine($"Sending a letter to the {model.Email} email...");

            var outgoingEmailForm = new OutgoingEmailForm(model);
            outgoingEmailForm.SetNotificationConfig(_configuration);

            _httpClient.BaseAddress = new Uri(_configuration.GetValue<string>("OutgoingEmailService:host"));
            const string endpoint = "/api/email/send-email";
            var result = await SendHttpRequest<object>(endpoint, HttpMethod.Post, null, outgoingEmailForm);
            if (result != null)
                Console.WriteLine($"A letter to the {model.Email} email sent successfully!");
            else
                Console.WriteLine($"Error of a letter sending to the {model.Email} email!!!");

            Console.WriteLine("The process of sending the letter has been completed.");
        }

        private async Task<TResponse> SendHttpRequest<TResponse>(string _api, HttpMethod _httpMethod, string _token = null, object _requestData = null) where TResponse : class
        {
            var request = new HttpRequestMessage(_httpMethod, _api);
            if (_token != null)
            {
                _token = _token.Replace("Bearer ", "");
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _token);
            }
            if (_requestData != null)
            {
                var json = JsonConvert.SerializeObject(_requestData);
                request.Content = new StringContent(json, Encoding.UTF8, "application/json");
            }

            HttpResponseMessage response;
            try
            {
                response = await _httpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false);
            }
            catch (System.Net.Http.HttpRequestException ex)
            {
                Console.WriteLine($"Error message: {ex.Message}");
                return null;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine($"Error message: {ex.Message}");
                return null;
            }

            if (response.IsSuccessStatusCode)
            {
                var contentStream = await response.Content.ReadAsStringAsync();
                TResponse result = JsonConvert.DeserializeObject<TResponse>(contentStream, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
                return (TResponse)result;
            }
            else
            {
                System.Console.WriteLine("------------->>>");
                System.Console.WriteLine(response.StatusCode);
                System.Console.WriteLine(response.ReasonPhrase);
                var contentStream = await response.Content.ReadAsStringAsync();
                System.Console.WriteLine(contentStream);
                return null;
            }
        }
    }
}