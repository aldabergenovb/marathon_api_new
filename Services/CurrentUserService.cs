using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using pmo_api.Constants;
using pmo_api.Models.Base;
using System.Threading.Tasks;
using pmo_api.Exceptions;
using Microsoft.AspNetCore.Identity;

namespace pmo_api.Services
{
    public interface ICurrentUserService
    {
        Guid? UserId { get; }
        string Role { get; }
    }

    public class CurrentUserService : ICurrentUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor ?? throw new ArgumentNullException(nameof(httpContextAccessor));
        }
        private Guid? GetUserGuid()
        {
            if (Guid.TryParse(_httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier), out Guid userGuid))
                return userGuid;
            return null;
        }
        public Guid? UserId => GetUserGuid();

        public string Role => _httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.Role);
    }
}