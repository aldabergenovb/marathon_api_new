pipeline {
    
   agent {   
      label 'node2'
   } 
   environment {
      HOME = '/tmp' 
      IP_PROD = '176.98.232.179'
      IP_DEV = '176.98.232.179'
      USER_PROD = 'root' 
      USER_DEV = 'root' 
      TARGET_PROD = '/var/www/api/'
      TARGET_DEV = '/var/www/api/'
      PROD_SERVICE = 'kestrel-pmo'
      DEV_SERVICE = 'kestrel-pmo'
      PORT_PROD = '9926'
      PORT_DEV = '9927'
      BUILD_PATH = '@2/bin/Release/net6.0/publish/'
      BRANCH_PROD = 'main'
      BRANCH_DEV = 'dev'
    } 
    stages {  
        stage('get_commit_details') {            
            steps {
              script {
                env.GIT_COMMIT_MSG = sh (script: 'git log -1 --pretty=%B ${GIT_COMMIT} | sed s/" "/%20/g | sed /^$/d | tr -d "\n" | tr -d "."', returnStdout: true).trim()
                env.GIT_AUTHOR = sh (script: 'git log -1 --pretty=%cn ${GIT_COMMIT} | sed s/" "/_/g', returnStdout: true).trim()
                env.projectNameProd= sh (script: "echo $JOB_NAME | sed 's/_prod//g'", returnStdout: true).trim()
                env.projectNameDev=sh (script:  "echo $JOB_NAME | sed 's/_dev//g'", returnStdout: true).trim()
              }
            }
        } 
        stage('RevertBuild') { 
            when { 
                not { 
                    expression {
                        return env.GIT_BRANCH == "origin/${BRANCH_PROD}"   
                    } 
                }
                not {
                    expression {
                        return env.GIT_BRANCH == "origin/${BRANCH_DEV}"   
                    }        
                } 
            }
            agent {      
              docker { 
                image 'mcr.microsoft.com/dotnet/sdk:6.0'
                label 'node2'
                }
            }              
            steps {                
                script {             
                    PrintStage()
                    updateGitlabCommitStatus name: 'BuildProduction', state: 'pending'
                    echo "environment - Production"                
                    echo "start publish"                
                    sh '/usr/bin/dotnet publish /p:Configuration=Release /p:EnvironmentName=Production'               
                    echo "end publish"   
                    updateGitlabCommitStatus name: 'BuildProduction', state: 'success'                 
                }                
            }   
        }           
        stage('BuildProduction') { 
            when {             
                expression {
                    return env.GIT_BRANCH == "origin/${BRANCH_PROD}"                    
                }            
            } 
            agent {      
              docker { 
                image 'mcr.microsoft.com/dotnet/sdk:6.0'
                label 'node2'
                }
            }   
            
            steps {
                PrintStage()
                updateGitlabCommitStatus name: 'BuildProduction', state: 'pending'
                script { 
                    echo "environment - Production"                
                    echo "start publish"                
                    sh '/usr/bin/dotnet publish /p:Configuration=Release /p:EnvironmentName=Production'               
                    echo "end publish"   
                    updateGitlabCommitStatus name: 'BuildProduction', state: 'success'                             
                }
            }   
        }        
        stage('BuildDevelopment') {  
            when {             
               expression {
                    return env.GIT_BRANCH == "origin/${BRANCH_DEV}"
                }             
            }
            agent {      
              docker { 
                image 'mcr.microsoft.com/dotnet/sdk:6.0'
                label 'node2'
                }
            }   
            
            steps {
                PrintStage()
                updateGitlabCommitStatus name: 'BuildDevelopment', state: 'pending'
                script { 
                    echo "environment - Development"                
                    echo "start publish"                
                    sh '/usr/bin/dotnet publish /p:Configuration=Release /p:EnvironmentName=Test'               
                    echo "end publish"   
                    updateGitlabCommitStatus name: 'BuildDevelopment', state: 'success'                             
                }
            }   
        } 
        stage('DeployProductionRever') { 
            when { 
                not { 
                    expression {
                        return env.GIT_BRANCH == "origin/${BRANCH_PROD}"   
                    } 
                }
                not {
                    expression {
                        return env.GIT_BRANCH == "origin/${BRANCH_DEV}"   
                    }         
                } 
            }
            steps {
                script {
                            
                    PrintStage()
                    updateGitlabCommitStatus name: 'Deploy', state: 'pending'
                    
                        echo "start deploy"
                        sh 'rsync -avPe "ssh -p ${PORT_PROD}" --delete /var/lib/jenkins/workspace/${JOB_NAME}${BUILD_PATH}. ${USER_PROD}@${IP_PROD}:${TARGET_PROD}'
                        echo "start restart ${PROD_SERVICE}"
                        sh 'ssh -p ${PORT_PROD} ${USER_PROD}@${IP_PROD} "systemctl restart ${PROD_SERVICE}"'
                        echo "end deploy"     
                        updateGitlabCommitStatus name: 'Deploy', state: 'success'        
                        // test 1234              
                }   
            }          
        }           
        stage('DeployProduction') { 
            when {             
                expression {
                    return env.GIT_BRANCH == "origin/${BRANCH_PROD}"
                }                           
            } 
            steps {
                script {
                            
                    PrintStage()
                    updateGitlabCommitStatus name: 'Deploy', state: 'pending'
                    
                        echo "start deploy"
                        sh 'rsync -avPe "ssh -p ${PORT_PROD}" --delete /var/lib/jenkins/workspace/${JOB_NAME}${BUILD_PATH}. ${USER_PROD}@${IP_PROD}:${TARGET_PROD}'
                        echo "start restart ${PROD_SERVICE}"
                        sh 'ssh -p ${PORT_PROD} ${USER_PROD}@${IP_PROD} "systemctl restart ${PROD_SERVICE}"'
                        echo "end deploy"     
                        updateGitlabCommitStatus name: 'Deploy', state: 'success'        
                        // test 1234              
                }   
            }          
        } 
        stage('DeployDevelopment') { 
            when {             
               expression {
                    return env.GIT_BRANCH == "origin/${BRANCH_DEV}"
                }            
            } 
            steps {
            script {
                        
                PrintStage()
                updateGitlabCommitStatus name: 'Deploy', state: 'pending'
                
                    echo "start deploy"
                    sh 'rsync -avPe "ssh -p ${PORT_DEV}" --delete /var/lib/jenkins/workspace/${JOB_NAME}${BUILD_PATH}. ${USER_DEV}@${IP_DEV}:${TARGET_DEV}'
                    echo "start restart ${DEV_SERVICE}"
                    sh 'ssh -p ${PORT_DEV} ${USER_DEV}@${IP_DEV} "systemctl restart ${DEV_SERVICE}"'
                    echo "end deploy"     
                    updateGitlabCommitStatus name: 'Deploy', state: 'success'        
                    // test 1234              
            }   
            }          
        } 
    }
    post {     
        success { 
                script {
                   if (env.GIT_BRANCH == "origin/${BRANCH_PROD}") {
                        echo 'SUCCESS'                        
                        sh 'curl "https://api.telegram.org/bot${TelegramToken}/sendMessage?chat_id=${Chat_ID}&parse_mode=HTML&text=Pipeline%20<b>$JOB_NAME</b>%0ABUILD_URL%20-%20"$BUILD_URL"%0AGIT_AUTHOR%20-%20"${GIT_AUTHOR}"%0AGIT_COMMIT_MSG%20-%20"${GIT_COMMIT_MSG}"%0ABUILD_STATUS%20-%20SUCCESS%0A%23$projectNameProd%20%23prod"'
                        }
                    else {                        
                        echo 'SUCCESS'                     
                        sh 'curl "https://api.telegram.org/bot${TelegramToken}/sendMessage?chat_id=${Chat_ID}&parse_mode=HTML&text=Pipeline%20<b>$JOB_NAME</b>%0ABUILD_URL%20-%20"$BUILD_URL"%0AGIT_AUTHOR%20-%20"${GIT_AUTHOR}"%0AGIT_COMMIT_MSG%20-%20"${GIT_COMMIT_MSG}"%0ABUILD_STATUS%20-%20SUCCESS%0A%23$projectNameDev%20%23dev"'
                    }
                }
        }                       
        failure { 
                script {
                   if (env.GIT_BRANCH == "origin/${BRANCH_PROD}") {
                        echo 'FAILURE'
                        sh 'curl "https://api.telegram.org/bot${TelegramToken}/sendMessage?chat_id=${Chat_ID}&parse_mode=HTML&text=Pipeline%20<b>$JOB_NAME<b>%0ABUILD_URL%20-%20"$BUILD_URL"%0AGIT_AUTHOR%20-%20"${GIT_AUTHOR}"%0AGIT_COMMIT_MSG%20-%20"${GIT_COMMIT_MSG}"%0ABUILD_STATUS%20-%20FAILURE%0A%23$projectNameProd%20%23prod"'
                }
                else {
                        echo 'FAILURE'
                        sh 'curl "https://api.telegram.org/bot${TelegramToken}/sendMessage?chat_id=${Chat_ID}&parse_mode=HTML&text=Pipeline%20<b>$JOB_NAME<b>%0ABUILD_URL%20-%20"$BUILD_URL"%0AGIT_AUTHOR%20-%20"${GIT_AUTHOR}"%0AGIT_COMMIT_MSG%20-%20"${GIT_COMMIT_MSG}"%0ABUILD_STATUS%20-%20FAILURE%0A%23$projectNameDev%20%23dev"'
                }
                }
        }        
    }
          
}


// Метод, который принимает на вход один текстовый аргумент. По умолчанию аргумент пустой
void PrintStage(String text=""){
	// Применение тернарного оператора
	// Если аргумент text пустой, в консоль Jenkins будет выведено десять звёздочек, название этапа, десять звёздочек
	// Если аргумент text не пустой, в консоль Jenkins будет выведено его значение
    text=="" ? println ('* '*10 + env.STAGE_NAME.toUpperCase() + " *"*10) : println (text)
}
