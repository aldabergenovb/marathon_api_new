using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.PostgreSql;
using pmo_api.Helpers;
using pmo_api.Hubs;
using pmo_api.Models.Base;
using pmo_api.Repositories;
using pmo_api.Services;
using pmo_api.Services.Policies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Converters;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Net.Http.Headers;
using System.Threading;

namespace pmo_api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvcCore().AddApiExplorer();
            services.AddControllers().AddNewtonsoftJson(jsonOptions =>
            {
                jsonOptions.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                jsonOptions.SerializerSettings.Converters.Add(new StringEnumConverter());
            });
            services.AddApiVersioning(s =>
            {
                // api-deprecated-versions
                // api-supported-versions
                s.ApiVersionReader = new HeaderApiVersionReader("api-version");
                s.AssumeDefaultVersionWhenUnspecified = true;
                s.DefaultApiVersion = new ApiVersion(1, 0);
            });

            string appDbConnection = Configuration["ConnectionStrings:AppDbConnection"];
            services.AddDbContext<AppDbContext>(options =>
                    options.UseNpgsql(appDbConnection,
                        b => b.MigrationsAssembly(typeof(AppDbContext).Assembly.FullName))
                        .UseLazyLoadingProxies()
                        , ServiceLifetime.Transient);

            services.AddScoped<IAppDbContext>(provider => provider.GetService<AppDbContext>());
            services.AddIdentity<AppUser, AppRole>()
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options => options.Password = AccountHelper.GetPasswordOptions());

            services.AddTransient<IAccountRepository, AccountRepository>();
            services.AddTransient<IMarathonUsersRepository, MarathonUsersRepository>();
            services.AddSingleton<ICurrentUserService, CurrentUserService>();
            services.AddSingleton<HttpClientPolicy>(new HttpClientPolicy());

            HttpClientHandler clientHandler = new()
            {
                ServerCertificateCustomValidationCallback = (_, __, ___, ____) => true
            };
            services.AddHttpClient<IOutgoingEmailService, OutgoingEmailService>("OutgoingEmailService", client =>
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("User-Agent", "marrathon_api");
            });

            // ===== Add Jwt Authentication ========
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear(); // remove default claims
            services
                .AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = Configuration["JwtIssuer"],
                        ValidAudience = Configuration["JwtIssuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtKey"])),
                        ClockSkew = TimeSpan.Zero // remove delay of token when expire
                    };
                    cfg.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            var path = context.HttpContext.Request.Path;
                            if (path.StartsWithSegments("/hubs/voip") || path.StartsWithSegments("/hubs/modulechat") || path.StartsWithSegments("/hubs/unseenrequests"))
                            {
                                var accessToken = context.Request.Query["access_token"];
                                if (!string.IsNullOrEmpty(accessToken))
                                    context.Token = accessToken;
                            }
                            return Task.CompletedTask;
                        }
                    };
                });

            services.AddAutoMapper(typeof(Startup).Assembly);
            services.AddSignalR().AddNewtonsoftJsonProtocol();

            services.AddVersionedApiExplorer(options =>
            {
                // add the versioned api explorer, which also adds IApiVersionDescriptionProvider service
                // note: the specified format code will format the version as "'v'major[.minor][-status]"
                options.GroupNameFormat = "'v'VVV";

                // note: this option is only necessary when versioning by url segment. the SubstitutionFormat
                // can also be used to control the format of the API version in route templates
                options.SubstituteApiVersionInUrl = true;
            });
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();

            services.AddSwaggerGen(options =>
            {
                options.OperationFilter<BasicAuthOperationsFilter>();
                options.DocumentFilter<SwaggerFilterOutControllers>();
            });
            services.AddSwaggerGenNewtonsoftSupport();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IApiVersionDescriptionProvider provider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // if (!env.IsEnvironment("Production"))    // TODO: disabled for allow upload users
            {
                app.UseSwagger(c => c.RouteTemplate = "/api/swagger/swagger/{documentName}/swagger.json");
                app.UseSwaggerUI(c =>
                {
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        c.SwaggerEndpoint($"swagger/{description.GroupName}/swagger.json", $"AqmolaMarathon {description.GroupName}");
                    }
                    c.RoutePrefix = "api/swagger";
                });
            }

            app.Use((context, next) =>
                {
                    var userLangs = context.Request.Headers["Accept-Language"].ToString();
                    var lang = "ru"; //default
                    if (userLangs == "kz" || userLangs == "en")
                        lang = userLangs;

                    Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(lang);
                    Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;

                    //save for later use
                    context.Items["ClientLang"] = lang;
                    context.Items["ClientCulture"] = Thread.CurrentThread.CurrentUICulture.Name;

                    return next();
                });

            app.UseCustomExceptionHandler();

            app.UseCors(builder => builder
                .SetIsOriginAllowed(_ => true)
                .AllowCredentials()
                .AllowAnyHeader()
                .AllowAnyMethod()
            );

            // app.UseHttpsRedirection();
            app.UseRouting();
            app.UseStatusCodePages();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<VoipHub>("/hubs/voip");
                endpoints.MapHub<ModuleChatHub>("/hubs/modulechat");
                endpoints.MapHub<UnseenRequestsHub>("/hubs/unseenrequests");
            });

            // RecurringJob.AddOrUpdate(() => contractUpdateService.SendContractsExpirationNotification(), "0 9 * * *", TimeZoneInfo.Local);
        }

        private class BasicAuthOperationsFilter : IOperationFilter
        {
            public void Apply(OpenApiOperation operation, OperationFilterContext context)
            {
                var noAuthRequired = context.ApiDescription.CustomAttributes().Any(attr => attr.GetType() == typeof(AllowAnonymousAttribute));
                if (!noAuthRequired)
                {
                    operation.Security = new List<OpenApiSecurityRequirement>
                    {
                        new OpenApiSecurityRequirement()
                        {
                            {
                                new OpenApiSecurityScheme
                                {
                                    Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" },
                                    Scheme = "oauth2",
                                    Name = "Bearer",
                                    In = ParameterLocation.Header,
                                },
                                new List<string>()
                            }
                        }
                    };
                }
            }
        }

        public class ConfigureSwaggerOptions : IConfigureOptions<SwaggerGenOptions>
        {
            private readonly IApiVersionDescriptionProvider _provider;

            public ConfigureSwaggerOptions(IApiVersionDescriptionProvider provider)
            {
                _provider = provider;
            }

            public void Configure(SwaggerGenOptions options)
            {
                // add a swagger document for each discovered API version
                // note: you might choose to skip or document deprecated API versions differently
                foreach (var description in _provider.ApiVersionDescriptions)
                {
                    options.SwaggerDoc(description.GroupName, CreateInfoForApiVersion(description));
                }

                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme.
                        Enter 'Bearer' [space] and then your token in the text input below.
                        Example: \""Bearer 12345abcdef\""",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);
            }

            private static OpenApiInfo CreateInfoForApiVersion(ApiVersionDescription description)
            {
                var info = new OpenApiInfo()
                {
                    Title = "Aqmola-Marathon",
                    Version = description.ApiVersion.ToString(),
                };

                if (description.IsDeprecated)
                {
                    info.Description += " This API version has been deprecated.";
                }

                return info;
            }
        }

        public class SwaggerFilterOutControllers : IDocumentFilter
        {
            public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
            {
                var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

                if (env == "Development")
                    return;

                if (env == "Test")
                    return;

                // if (env == "Production")
                //     return;

                // var allowPaths = new List<string>() { "/_Scripts" };
                // List<string> pathKeysToRemove = swaggerDoc.Paths.Where(x => !allowPaths.Any(e => x.Key.Contains(e))).Select(x => x.Key).ToList();
                // foreach (var pathKey in pathKeysToRemove)
                // {
                //     swaggerDoc.Paths.Remove(pathKey);
                // }
            }
        }
    }
}
