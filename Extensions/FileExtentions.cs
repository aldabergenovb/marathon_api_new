using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using pmo_api.Models.Base;
using pmo_api.Models.Files;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace pmo_api.Extensions
{
    public static class FileExtentions
    {
        #region RemoteFile
        public static FileContentResult ToFileContentResult(this RemoteFileContent source)
        {
            return new FileContentResult(source.Content, source.Extension)
            {
                FileDownloadName = source.Name,
            };
        }

        public static async Task<RemoteFileContent> ToRemoteFileAsync(this IFormFile source, AppUser uploadedUser, string role)
        {
            var scan = new RemoteFileContent();

            using (var memoryStream = new MemoryStream())
            {
                await source.CopyToAsync(memoryStream);
                memoryStream.Position = 0;
                scan.Content = memoryStream.ToArray();
                scan.Name = source.FileName;
                scan.Extension = source.ContentType;
                scan.Size = source.Length;
                scan.UploadedById = uploadedUser.Id;
                scan.UploadedByRole = role;
            }

            return scan;
        }

        public static RemoteFileContent ToRemoteFile(this IFormFile source, AppUser uploadedByUser)
        {
            var scan = new RemoteFileContent();

            using (var memoryStream = new MemoryStream())
            {
                source.CopyTo(memoryStream);
                memoryStream.Position = 0;
                scan.Content = memoryStream.ToArray();
                scan.Name = source.FileName;
                scan.Extension = source.ContentType;
                scan.Size = source.Length;
                scan.UploadedById = uploadedByUser.Id;
            }

            return scan;
        }

        public static FileBase64Content ToFileBase64Content(this RemoteFileContent source)
        {
            return new FileBase64Content()
            {
                FileName = source.Name,
                Size = source.Size,
                Base64string = Convert.ToBase64String(source.Content),
                Extension = source.Extension
            };
        }

        #endregion

        #region IFormFile
        public static StreamContent ToStreamContent(this IFormFile source)
        {
            var streamContent = new StreamContent(source.OpenReadStream());
            streamContent.Headers.ContentType = new MediaTypeHeaderValue(source.ContentType);
            return streamContent;
        }

        public static string ToBase64String(this IFormFile source)
        {
            var result = string.Empty;
            using (var ms = new MemoryStream())
            {
                source.CopyTo(ms);
                var fileBytes = ms.ToArray();
                result = Convert.ToBase64String(fileBytes);
            }
            return result;
        }

        #endregion
    }
}