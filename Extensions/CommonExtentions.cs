using System.Text.Json;

namespace pmo_api.Extensions
{
    public static class CommonExtentions
    {
        public static T Duplicate<T>(this T source)
        {
            var serialized = JsonSerializer.Serialize(source);
            return JsonSerializer.Deserialize<T>(serialized);
        }
    }
}