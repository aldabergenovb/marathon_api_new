using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using pmo_api.Helpers;
using pmo_api.Models.Base;
using pmo_api.Models.Directories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using pmo_api.Models.Filters;
using pmo_api.Models.MarathonUserss;
using System.Collections.Generic;
using marathon_api_new.Services;
using System.Configuration;
using Microsoft.Extensions.Configuration;

namespace pmo_api.Repositories
{
    public interface IMarathonUsersRepository
    {
        Task<ActionMethodResult> GetAsync(Guid id, ClaimsPrincipal userClaims, string lang);
        Task<ActionMethodResult> GetAllPaginatedAsync(MarathonUsersFilters options, ClaimsPrincipal userClaims, string lang);
        Task<ActionMethodResult> GetAllPaginatedMainListAsync(MarathonUsersFilters options, ClaimsPrincipal userClaims, string lang);
        Task<ActionMethodResult> CreateAsync(MarathonUsersCreateViewModel model, ClaimsPrincipal userClaims, string lang);
        Task<ActionMethodResult> PutAsync(MarathonUsersPutViewModel model, ClaimsPrincipal userClaims, string lang);
        Task<ActionMethodResult> DeleteAsync(Guid id, ClaimsPrincipal userClaims, string lang);
        Task<ActionMethodResult> DeleteListAsync(List<Guid> idList, ClaimsPrincipal userClaims, string lang);
    }

    public class MarathonUsersRepository : IMarathonUsersRepository
    {
        private readonly AppDbContext _db;
        private readonly IMapper _mapper;
        private readonly UserManager<AppUser> _userManager;
        private readonly IConfiguration _configuration;

        public MarathonUsersRepository(
            AppDbContext db,
            IMapper mapper,
            UserManager<AppUser> userManager
            , IConfiguration configuration)
        {
            _db = db;
            _mapper = mapper;
            _userManager = userManager;
            _configuration = configuration;
        }

        public async Task<ActionMethodResult> GetAsync(Guid id, ClaimsPrincipal userClaims, string lang)
        {
            var account = await AccountHelper.CheckUserClaims(_userManager, userClaims, lang);
            if (account == null) return ActionMethodResult.Error(null, MessageHelper.AccessDenied(lang));

            var marathonUsers = await _db.MarathonUsers.AsSplitQuery().FirstOrDefaultAsync(x => x.Id == id);
            if (marathonUsers is null)
                return ActionMethodResult.Error(null, MessageHelper.DataNotFound(lang));

            var viewModel = _mapper.Map<MarathonUsersViewModel>(marathonUsers);
            return ActionMethodResult.Success(viewModel);
        }

        public async Task<ActionMethodResult> PutAsync(MarathonUsersPutViewModel model, ClaimsPrincipal userClaims, string lang)
        {
            var account = await AccountHelper.CheckUserClaims(_userManager, userClaims, lang);
            if (account == null) return ActionMethodResult.Error(null, MessageHelper.AccessDenied(lang));

            var marathonUsers = await _db.MarathonUsers.AsSplitQuery().FirstOrDefaultAsync(x => x.Id == model.Id);
            if (marathonUsers is null)
                return ActionMethodResult.Error(null, MessageHelper.DataNotFound(lang));

            marathonUsers.IsPaid = model.IsPaid;

            _db.MarathonUsers.Update(marathonUsers);
            await _db.SaveChangesAsync();

            var viewModel = _mapper.Map<MarathonUsersViewModel>(marathonUsers);
            return ActionMethodResult.Success(viewModel, MessageHelper.SuccessSaved(lang));
        }

        public async Task<ActionMethodResult> GetAllPaginatedAsync(MarathonUsersFilters options, ClaimsPrincipal userClaims, string lang)
        {
            var account = await AccountHelper.CheckUserClaims(_userManager, userClaims, lang);
            if (account == null) return ActionMethodResult.Error(null, MessageHelper.AccessDenied(lang));

            IQueryable<MarathonUsers> marathonUsers = _db.MarathonUsers.Where(x => !x.IsDeleted);

            //Filters, Sorting
            marathonUsers = marathonUsers.ApplyRoleFilter(account.Role, account.User.Id);
            marathonUsers = marathonUsers.ApplyFilters(options);
            marathonUsers = marathonUsers.ApplySorting(options.OrderBy);

            var paginationModel = new Pagination<MarathonUsersViewModel, MarathonUsers>(c => _mapper.Map<MarathonUsersViewModel>(c), marathonUsers, options);
            return ActionMethodResult.Success(paginationModel);
        }

        public async Task<ActionMethodResult> GetAllPaginatedMainListAsync(MarathonUsersFilters options, ClaimsPrincipal userClaims, string lang)
        {
            var account = await AccountHelper.CheckUserClaims(_userManager, userClaims, lang);
            if (account == null) return ActionMethodResult.Error(null, MessageHelper.AccessDenied(lang));

            List<MarathonUsers> marathonUsers = _db.MarathonUsers
                                                .Where(x => !x.IsDeleted)
                                                .GroupBy(x => x.Email)
                                                .Select(g => g.First())
                                                .ToList();
            IQueryable<MarathonUsers> data = marathonUsers.AsQueryable();
            //Filters, Sorting
            data = data.ApplyRoleFilter(account.Role, account.User.Id);
            data = data.ApplyFilters(options);
            data = data.ApplySorting(options.OrderBy);

            var paginationModel = new Pagination<MarathonUsersMainPageViewModel, MarathonUsers>(c => _mapper.Map<MarathonUsersMainPageViewModel>(c), data, options);
            return ActionMethodResult.Success(paginationModel);
        }

        public static List<T> removeDuplicates<T>(List<T> list)
        {
            return new HashSet<T>(list).ToList();
        }

        public async Task<ActionMethodResult> CreateAsync(MarathonUsersCreateViewModel model, ClaimsPrincipal userClaims, string lang)
        {
            MarathonUsers marathonUsers = new()
            {
                Email = model.Email,
                PhoneNumber = model.PhoneNumber,
                FirstName = model.FirstName,
                LastName = model.LastName,
                BirthDate = model.BirthDate,
                GenderName = model.GenderName,
                CountryName = model.CountryName,
                CityName = model.CityName,
                ExtraContact = model.ExtraContact,
                Size = model.Size,
                Distance = model.Distance,
                marathonEnum = model.marathonEnum
            };

            await _db.MarathonUsers.AddAsync(marathonUsers);
            await _db.SaveChangesAsync();

            #region EmailSend
            string host = _configuration.GetValue<string>("OutgoingEmailServer:host");
            string port = _configuration.GetValue<string>("OutgoingEmailServer:port");
            string Password = _configuration.GetValue<string>("OutgoingEmailServer:notification:pass");
            string EmailFrom = _configuration.GetValue<string>("OutgoingEmailServer:notification:from");
            string Title = _configuration.GetValue<string>("OutgoingEmailServer:notification:title");
            string body = _configuration.GetValue<string>("OutgoingEmailServer:body");
            var emailService = new EmailService(host, 587, EmailFrom, Password);
            emailService.SendEmail(model, Title, body);
            #endregion

            return ActionMethodResult.Success(marathonUsers.Id, MessageHelper.SuccessSaved(lang));
        }

        public async Task<ActionMethodResult> DeleteAsync(Guid id, ClaimsPrincipal userClaims, string lang)
        {
            var account = await AccountHelper.CheckUserClaims(_userManager, userClaims, lang);
            if (account == null) return ActionMethodResult.Error(null, MessageHelper.AccessDenied(lang));

            var marathonUsers = _db.MarathonUsers.FirstOrDefault(x => x.Id == id);

            _db.MarathonUsers.Remove(marathonUsers);
            await _db.SaveChangesAsync();

            return ActionMethodResult.Success(null, MessageHelper.SuccessDeleted(lang));
        }

        public async Task<ActionMethodResult> DeleteListAsync(List<Guid> idList, ClaimsPrincipal userClaims, string lang)
        {
            var account = await AccountHelper.CheckUserClaims(_userManager, userClaims, lang);
            if (account == null) return ActionMethodResult.Error(null, MessageHelper.AccessDenied(lang));

            var marathonUserss = _db.MarathonUsers.Where(x => idList.Contains(x.Id));

            _db.MarathonUsers.RemoveRange(marathonUserss);
            await _db.SaveChangesAsync();

            return ActionMethodResult.Success(null, MessageHelper.SuccessDeleted(lang));
        }
    }
}