using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using pmo_api.Constants;
using pmo_api.Helpers;
using pmo_api.Models.Base;
using pmo_api.Models.Directories;
using pmo_api.Models.Users;
using pmo_api.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace pmo_api.Repositories
{
    public interface IAccountRepository
    {
        Task<ActionMethodResult> LoginAsync(LoginForm model, string lang);
        Task<ActionMethodResult> ConfirmEmailAsync(ConfirmEmailForm model, string lang);
        Task<ActionMethodResult> ForgotPasswordAsync(ForgotPasswordForm model, string lang);
        Task<ActionMethodResult> ResetPasswordAsync(ResetPasswordForm model, string lang);
        Task<ActionMethodResult> GetAsync(ClaimsPrincipal userClaims, string lang);
        Task<ActionMethodResult> PutAsync(AppUserViewModel model, ClaimsPrincipal userClaims, string lang);
        Task<ActionMethodResult> ChangePasswordAsync(ChangePasswordForm model, ClaimsPrincipal userClaims, string lang);
        // Task<ActionMethodResult> RegisterUserAsync(AppUserRegisterViewModel model, ClaimsPrincipal userClaims, string lang);
    }

    public class AccountRepository : IAccountRepository
    {
        private readonly AppDbContext _db;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly IOutgoingEmailService _emailService;

        public AccountRepository(
            AppDbContext db,
            IMapper mapper,
            IConfiguration configuration,
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager,
            IOutgoingEmailService emailService
            )
        {
            _db = db;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _emailService = emailService;
        }

        public async Task<ActionMethodResult> LoginAsync(LoginForm model, string lang)
        {
            model.Login = model.Login.ToLower();
            var user = await _userManager.FindByNameAsync(model.Login);
            if (user == null)
                return ActionMethodResult.Error(ActionMethodConstants.Results.NotFound, "", MessageHelper.UserNotFound(lang));

            if (!await _userManager.IsEmailConfirmedAsync(user))
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, "", MessageHelper.EmailNotConfirmed(lang));

            if (user.IsDeactivated)
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, "", MessageHelper.UserDeactivated(lang));

            var roles = await _userManager.GetRolesAsync(user);
            roles.Add("norole");

            var result = await _signInManager.PasswordSignInAsync(model.Login, model.Password, false, false);
            if (result.Succeeded)
            {
                var refreshToken = _db.RefreshTokens.Find(user.Id);
                if (refreshToken == null)
                {
                    refreshToken = new RefreshToken(user.Id);
                    _db.RefreshTokens.Add(refreshToken);
                }
                refreshToken.Token = Guid.NewGuid().ToString("N");
                refreshToken.StartDate = DateTime.Now;
                refreshToken.FinishDate = AccountHelper.GetJwtExpiresDateTimeByRole(_configuration);
                await _db.SaveChangesAsync();

                string organizationId = string.Empty;
                bool isHead = false;
                var pmoToken = AccountHelper.GenerateJwtToken(_configuration, user, roles[0], refreshToken.Token, organizationId, isHead, model.SavePassword);
                return ActionMethodResult.Success(pmoToken);
            }
            else
            {
                Console.WriteLine($"login failed: {model.Login} | {roles[0]}");
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, "", MessageHelper.PasswordIncorrectAuth(lang));
            }
        }

        public async Task<ActionMethodResult> ConfirmEmailAsync(ConfirmEmailForm model, string lang)
        {
            var user = await _userManager.FindByIdAsync(model.UserId);
            if (user == null)
                return ActionMethodResult.Error(ActionMethodConstants.Results.NotFound, "", MessageHelper.UserNotFound(lang));

            if (user.IsDeactivated)
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, "", MessageHelper.UserDeactivated(lang));

            if (user.EmailConfirmed)
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, "", MessageHelper.EmailIsConfirmed(lang));

            var result = await _userManager.ConfirmEmailAsync(user, model.Code);
            if (result.Succeeded)
                return ActionMethodResult.Success(null, MessageHelper.UserAccessSuccess(lang));
            else
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, "", MessageHelper.ActivationTermExpired(lang));
        }

        public async Task<ActionMethodResult> ForgotPasswordAsync(ForgotPasswordForm model, string lang)
        {
            model.Email = model.Email.ToLower();
            var user = await _userManager.FindByNameAsync(model.Email);
            if (user == null)
                return ActionMethodResult.Error(ActionMethodConstants.Results.NotFound, "", MessageHelper.UserNotFound(lang));

            if (!await _userManager.IsEmailConfirmedAsync(user))
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, "", MessageHelper.EmailNotConfirmed(lang));

            if (user.IsDeactivated)
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, "", MessageHelper.UserDeactivated(lang));

            string code = await _userManager.GeneratePasswordResetTokenAsync(user);
            var callbackUrl = $"{_configuration["web-url"]}/reset/{user.Id}/{code}";
            var outgoingEmail = AccountHelper.GetForgotPasswordMessage(user, callbackUrl, lang);
            await _emailService.SendNoReplyMailAsync(outgoingEmail);

            return ActionMethodResult.Success(null, MessageHelper.EmailConfirmMessage(lang));
        }

        public async Task<ActionMethodResult> ResetPasswordAsync(ResetPasswordForm model, string lang)
        {
            var user = await _userManager.FindByIdAsync(model.UserId);
            if (user == null)
                return ActionMethodResult.Error(ActionMethodConstants.Results.NotFound, "", MessageHelper.UserNotFound(lang));

            if (!await _userManager.IsEmailConfirmedAsync(user))
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, "", MessageHelper.EmailNotConfirmed(lang));

            if (user.IsDeactivated)
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, "", MessageHelper.UserDeactivated(lang));

            if (!model.PasswordValidation())
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, "", MessageHelper.PasswordNotMatch(lang));

            var passwordValidator = new PasswordValidator<AppUser>();
            var passwordValidatorResult = await passwordValidator.ValidateAsync(_userManager, user, model.Password);
            if (!passwordValidatorResult.Succeeded && passwordValidatorResult.Errors.Any())
            {
                foreach (var error in passwordValidatorResult.Errors)
                {
                    var value = UserConstants.UserManagerPasswordErrorCodes.FirstOrDefault(x => x == error.Code);
                    if (value != null)
                    {
                        var method = typeof(MessageHelper).GetMethod(value);
                        if (method != null)
                            return ActionMethodResult.Error(ActionMethodConstants.Results.InvalidData, "", method.Invoke(null, new object[] { lang }));
                    }
                    else
                    {
                        var message = string.Join(", ", passwordValidatorResult.Errors.Select(x => "Code: " + x.Code + " | Description: " + x.Description));
                        Console.WriteLine("----------> ResetPasswordAsync Error:");
                        Console.WriteLine(message);
                        return ActionMethodResult.Error(ActionMethodConstants.Results.ServerError, "", MessageHelper.PasswordNotMatch(lang));
                    }
                }
            }

            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            // await _irs.UserChangePassword(user.Email, model.Password);
            if (result.Succeeded)
            {
                return ActionMethodResult.Success(ActionMethodConstants.Results.Success, MessageHelper.PasswordChanged(lang), _configuration["web-url"]);
            }
            else
            {
                var message = string.Join(", ", result.Errors.Select(x => "Code: " + x.Code + " | Description: " + x.Description));
                Console.WriteLine("----------> ResetPasswordAsync Error:");
                Console.WriteLine(message);
                return ActionMethodResult.Error(ActionMethodConstants.Results.ServerError, "", MessageHelper.PasswordNotMatch(lang));
            }
        }

        public async Task<ActionMethodResult> GetAsync(ClaimsPrincipal userClaims, string lang)
        {
            var user = await _userManager.GetUserAsync(userClaims);
            if (user is null)
                return ActionMethodResult.Error(ActionMethodConstants.Results.AccessDenied, "", MessageHelper.AccessDenied(lang));

            var userRole = await _userManager.GetRolesAsync(user);
            if (userRole is null || userRole.Count == 0 || userRole[0] is null)
                return ActionMethodResult.Error(ActionMethodConstants.Results.AccessDenied, "", MessageHelper.AccessDenied(lang));

            var viewModel = _mapper.Map<AppUserViewModel>(user);
            viewModel.Role = userRole[0];
            return ActionMethodResult.Success(viewModel);
        }

        public async Task<ActionMethodResult> PutAsync(AppUserViewModel model, ClaimsPrincipal userClaims, string lang)
        {
            var user = await _userManager.GetUserAsync(userClaims);
            if (user is null)
                return ActionMethodResult.Error(ActionMethodConstants.Results.AccessDenied, "", MessageHelper.AccessDenied(lang));

            var userRole = await _userManager.GetRolesAsync(user);
            if (userRole is null || userRole.Count == 0 || userRole[0] is null)
                return ActionMethodResult.Error(ActionMethodConstants.Results.AccessDenied, "", MessageHelper.AccessDenied(lang));

            if (user.PhoneNumber != model.PhoneNumber && await _db.AppUsers.FindAsync(model.PhoneNumber) != null)
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, "", MessageHelper.PhoneNumberExist(lang));

            user.Update(model);

            var result = await _userManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                var userUpdateLog = AppUserLog.InstanceUpdateLog(user.Id, user.UserName, userRole[0], user.UserName, userRole?.FirstOrDefault());
                await _db.AppUserLogs.AddAsync(userUpdateLog);
                await _db.SaveChangesAsync();

                var viewModel = _mapper.Map<AppUserViewModel>(user);
                viewModel.Role = userRole[0];
                return ActionMethodResult.Success(viewModel, MessageHelper.ProfileChanged(lang));
            }
            else
            {
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, "", MessageHelper.ProfileSaveError(lang));
            }
        }

        public async Task<ActionMethodResult> ChangePasswordAsync(ChangePasswordForm model, ClaimsPrincipal userClaims, string lang)
        {
            var user = await _userManager.GetUserAsync(userClaims);
            if (user is null)
                return ActionMethodResult.Error(ActionMethodConstants.Results.AccessDenied, "", MessageHelper.AccessDenied(CultureConstants.Languages.Ru));

            var userRole = await _userManager.GetRolesAsync(user);
            if (userRole is null || userRole.Count == 0 || userRole[0] is null)
                return ActionMethodResult.Error(ActionMethodConstants.Results.AccessDenied, "", MessageHelper.AccessDenied(CultureConstants.Languages.Ru));

            if (!await _userManager.IsEmailConfirmedAsync(user))
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, "", MessageHelper.EmailNotConfirmed(lang));

            if (user.IsDeactivated)
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, "", MessageHelper.UserDeactivated(lang));

            if (!await _userManager.CheckPasswordAsync(user, model.CurrentPassword))
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, "", MessageHelper.PasswordIncorrect(lang));

            if (!model.PasswordValidation())
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, "", MessageHelper.PasswordNotMatch(lang));

            var result = await _userManager.ChangePasswordAsync(user, model.CurrentPassword, model.Password);
            user.Password = model.Password;
            // await _irs.UserChangePassword(user.Email, model.Password);
            await _db.SaveChangesAsync();
            if (result.Succeeded)
            {
                return ActionMethodResult.Success(ActionMethodConstants.Results.Success, "", MessageHelper.PasswordChanged(lang));
            }
            else
            {
                var errorMessage = AccountHelper.GetChangePasswordErrorMessage(result.Errors);
                return ActionMethodResult.Error(ActionMethodConstants.Results.Error, errorMessage);
            }
        }


        // public async Task<ActionMethodResult> RegisterUserAsync(AppUserRegisterViewModel model, ClaimsPrincipal userClaims, string lang)
        // {
        //     var user = await _userManager.GetUserAsync(userClaims);
        //     if (user is null)
        //         return ActionMethodResult.Error(ActionMethodConstants.Results.AccessDenied, "", MessageHelper.AccessDenied(lang));

        //     var userRole = await _userManager.GetRolesAsync(user);
        //     if (userRole is null || userRole.Count == 0 || userRole[0] is null)
        //         return ActionMethodResult.Error(ActionMethodConstants.Results.AccessDenied, "", MessageHelper.AccessDenied(lang));

        //     var userDublicate = await _userManager.FindByEmailAsync(model.Email);
        //     if (userDublicate != null)
        //         return ActionMethodResult.Error(ActionMethodConstants.Results.RecordIsExists, "", MessageHelper.EmailIsExist(lang));

        //     var newUserPassword = AccountHelper.GenerateRandomPassword();

        //     AppUser newUser = new()
        //     {
        //         Email = model.Email?.ToLower(),
        //         UserName = model.Email?.ToLower(),
        //         FirstName = model.FirstName,
        //         LastName = model.LastName,
        //         MiddleName = model.MiddleName,
        //         Password = newUserPassword,
        //         PositionName = model.PositionName,
        //         PositionNameKz = model.PositionNameKz
        //     };

        //     var link = _configuration["web-url"];
        //     var userCreation = await _userManager.CreateAsync(newUser, newUserPassword);
        //     if (userCreation.Succeeded)
        //     {
        //         newUser.Password = newUserPassword;
        //         var role = _db.Roles.FirstOrDefault(x => x.Name == model.Role)?.Name ?? model.Role;
        //         var roleAssignmentResult = await _userManager.AddToRoleAsync(newUser, model.Role);
        //         if (!roleAssignmentResult.Succeeded)
        //         {
        //             await _userManager.DeleteAsync(newUser);
        //             return ActionMethodResult.Error(ActionMethodConstants.Results.ServerError, "", MessageHelper.RegisterUserError(lang));
        //         }
        //         await _db.SaveChangesAsync();
        //         var registrationAlertEmail = AccountHelper.RegistrationAlert("", "", "", link, model.Email, lang);  // TODO: add user data
        //         BackgroundJob.Schedule(() => _emailService.SendMailAsync(registrationAlertEmail), TimeSpan.FromSeconds(2));
        //         return ActionMethodResult.Success(ActionMethodConstants.Results.Success, "", MessageHelper.RegisterSuccess(lang));
        //     }
        //     else
        //     {
        //         return ActionMethodResult.Error(ActionMethodConstants.Results.ServerError, "", MessageHelper.RegisterUserError(lang));
        //     }
        // }
    }
}