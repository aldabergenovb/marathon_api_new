using System.Collections.Generic;
using pmo_api.Extensions;
using pmo_api.Models.Files;

namespace pmo_api.Models.Emails
{
    public class OutgoingEmail
    {
        public OutgoingEmail() { }
        public OutgoingEmail(string email, string subject, string message)
        {
            Email = email;
            Subject = subject;
            Message = message;
        }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public ICollection<Base64Attachment> Attachments { get; set; } = new List<Base64Attachment>();
    }

    /// <summary>Файл в формате base64</summary>
    public class Base64Attachment
    {
        public Base64Attachment() { }
        public Base64Attachment(RemoteFileContent file)
        {
            Filename = file.Name;
            Content = file.ToFileBase64Content().Base64string;
        }

        /// <summary>Контент файла в формате base64</summary>
        public string Content { get; set; }

        /// <summary>Наименование файла</summary>
        public string Filename { get; set; }
        public OutgoingEmailFormAttachment CopyToOutgoingEmailFormAttachment()
        {
            return new OutgoingEmailFormAttachment(this.Filename, this.Content);
        }
    }
}