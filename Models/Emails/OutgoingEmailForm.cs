using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace pmo_api.Models.Emails
{
    public class OutgoingEmailForm
    {
        public OutgoingEmailForm() { }

        public OutgoingEmailForm(OutgoingEmail model)
        {
            EmailTo = model.Email;
            Subject = model.Subject;
            Message = model.Message;
            Html = model.Message;
            Attachments = model.Attachments
                .Select(x => x.CopyToOutgoingEmailFormAttachment())
                .ToList();
        }

        [JsonProperty("host")]
        public string Host { get; set; }

        [JsonProperty("port")]
        public int Port { get; set; }

        [JsonProperty("secure")]
        public bool Secure { get; set; } = true;    // true - enable ssl/tls. false - disable ssl/tls

        [JsonProperty("requireTLS")]
        public bool RequireTLS { get; set; }

        [JsonProperty("rejectUnauthorized")]
        public bool RejectUnauthorized { get; set; }

        [JsonProperty("from")]
        public string EmailFrom { get; set; }

        [JsonProperty("pass")]
        public string Password { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("to")]
        public string EmailTo { get; set; }

        [JsonProperty("subject")]
        public string Subject { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("html")]
        public string Html { get; set; }

        [JsonProperty("attachments")]
        public ICollection<OutgoingEmailFormAttachment> Attachments { get; set; } = new List<OutgoingEmailFormAttachment>();

        public void SetNoReplyConfig(IConfiguration configuration)
        {
            this.Host = configuration.GetValue<string>("NoReplyOutgoingEmailServer:host");
            this.Port = configuration.GetValue<int>("NoReplyOutgoingEmailServer:port");
            this.Secure = configuration.GetValue<bool>("NoReplyOutgoingEmailServer:secure");
            this.Title = configuration.GetValue<string>("NoReplyOutgoingEmailServer:no-reply:title");
            this.EmailFrom = configuration.GetValue<string>("NoReplyOutgoingEmailServer:no-reply:from");
            this.Password = configuration.GetValue<string>("NoReplyOutgoingEmailServer:no-reply:pass");
            this.RequireTLS = true;
            this.RejectUnauthorized = false;
        }

        public void SetNotificationConfig(IConfiguration configuration)
        {
            this.Host = configuration.GetValue<string>("OutgoingEmailServer:host");
            this.Port = configuration.GetValue<int>("OutgoingEmailServer:port");
            this.Secure = configuration.GetValue<bool>("OutgoingEmailServer:secure");
            this.Title = configuration.GetValue<string>("OutgoingEmailServer:notification:title");
            this.EmailFrom = configuration.GetValue<string>("OutgoingEmailServer:notification:from");
            this.Password = configuration.GetValue<string>("OutgoingEmailServer:notification:pass");
            this.RequireTLS = true;
            this.RejectUnauthorized = false;
        }
    }

    public class OutgoingEmailFormAttachment
    {
        public OutgoingEmailFormAttachment()
        {
            Encoding = "base64";
        }
        public OutgoingEmailFormAttachment(string fileName, string content) : this()
        {
            FileName = fileName;
            Content = content;
        }

        [JsonProperty("filename")]
        public string FileName { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("encoding")]
        public string Encoding { get; set; }
    }
}