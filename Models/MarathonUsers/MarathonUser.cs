using System;
using Newtonsoft.Json;
using pmo_api.Models.Common;
using pmo_api.Models.Enums;

namespace pmo_api.Models.MarathonUserss
{
    public class BaseMarathonUsers : AuditableEntity
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string GenderName { get; set; }
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string ExtraContact { get; set; }
        public string Size { get; set; }
        public string Distance { get; set; }
        public bool IsPaid { get; set; }
        public MarathonEnum marathonEnum { get; set; }
    }

    public class MarathonUsers : BaseMarathonUsers
    {
        [JsonProperty("id")]
        public Guid Id { get; set; } = Guid.NewGuid();
    }

    public class MarathonUsersViewModel
    {
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string GenderName { get; set; }
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string ExtraContact { get; set; }
        public string Size { get; set; }
        public string Distance { get; set; }
        public bool IsPaid { get; set; }
        public MarathonEnum marathonEnum { get; set; }
    }

    public class MarathonUsersMainPageViewModel
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string GenderName { get; set; }
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string Distance { get; set; }
        public MarathonEnum marathonEnum { get; set; }
    }

    public class MarathonUsersCreateViewModel
    {
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string GenderName { get; set; }
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string ExtraContact { get; set; }
        public string Size { get; set; }
        public string Distance { get; set; }
        public MarathonEnum marathonEnum { get; set; }
    }

    public class MarathonUsersPutViewModel
    {
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string GenderName { get; set; }
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string ExtraContact { get; set; }
        public string Size { get; set; }
        public string Distance { get; set; }
        public bool IsPaid { get; set; }
        public MarathonEnum marathonEnum { get; set; }
    }
}