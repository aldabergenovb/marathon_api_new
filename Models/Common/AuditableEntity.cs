using System;
using pmo_api.Models.Base;
using Microsoft.EntityFrameworkCore;
namespace pmo_api.Models.Common
{
    public abstract class AuditableEntity
    {
        /// <summary>Дата создания</summary>
        [Comment("Дата создания")]
        public DateTimeOffset Created { get; set; }
        /// <summary>ID Создателя</summary>
        [Comment("ID Создателя")]
        public Guid CreatedById { get; set; }
        /// <summary>Создатель</summary>
        [Comment("Создатель")]
        public virtual AppUser CreatedBy { get; set; }
        /// <summary>Дата последнего изменения</summary>
        [Comment("Дата последнего изменения")]
        public DateTimeOffset? LastModified { get; set; }
        /// <summary>ID автора который вносил последние изменения</summary>
        [Comment("ID автора который вносил последние изменения")]
        public Guid? LastModifiedById { get; set; }
        /// <summary>Автор последнего изменения</summary>
        [Comment("Автор последнего изменения")]
        public virtual AppUser LastModifiedBy { get; set; }
        /// <summary>Удаленс ли запись</summary>
        [Comment("Удаленс ли запись")]
        public bool IsDeleted { get; set; }
        /// <summary>Дата удаления</summary>
        [Comment("Дата удаления")]
        public DateTimeOffset? Deleted { get; set; }
        /// <summary>ID автора который удалил запись</summary>
        [Comment("ID автора который удалил запись")]
        public Guid? DeletedById { get; set; }
        /// <summary>Автор удаливший</summary>
        [Comment("Автор удаливший")]
        public virtual AppUser DeletedBy { get; set; }
    }
}