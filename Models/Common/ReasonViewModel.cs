using System;

namespace pmo_api.Models.Common
{
    public class ReasonViewModel
    {
        public DateTimeOffset Date { get; set; }
        public string Reason { get; set; }
        public string LogCode { get; set; }
    }
}