namespace pmo_api.Models.Directories
{
    public class Dropdown<TKey, TValue>
    {
        public TKey Key { get; set; }
        public string Text { get; set; }
        public TValue Value { get; set; }

        public Dropdown() { }
        public Dropdown(TKey key, string text, TValue value)
        {
            Key = key;
            Text = text;
            Value = value;
        }
    }
}