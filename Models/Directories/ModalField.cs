using System.Collections.Generic;
using Newtonsoft.Json;

namespace pmo_api.Models.Directories
{
    public class ModalField
    {
        public ModalField()
        {
            Id = 1;
            Required = false;
            Model = "";
            Name = "";
            Type = "";
            Label = "";
            PlaceHolder = "";
            Read = false;
            Value = "";
            Data = null;
            TableName = "";
            Columns = new List<ModalTableColumns>();
            MultipleResult = false;
        }

        public int Id { get; set; }
        public bool Required { get; set; }
        public string Model { get; set; }   // for object's inner models
        public string Name { get; set; }    // field name
        public string Type { get; set; }    // textarea, date, input, dropdown, table
        public string Label { get; set; }   // field ru name
        public string PlaceHolder { get; set; }
        public bool Read { get; set; }      // read only

        [JsonIgnore]
        public string Value { get; set; }
        public dynamic Data { get; set; }   // for table or dropdown
        public string TableName { get; set; }
        public ICollection<ModalTableColumns> Columns { get; set; } = new List<ModalTableColumns>();
        public bool MultipleResult { get; set; }    // for table or dropdown
    }

    public class ModalTableColumns
    {
        public string HeaderFilter { get; set; }
        public string Title { get; set; }
        public string Field { get; set; }
        public int Width { get; set; }

        [JsonProperty("formatter")]
        public string Formatter { get; set; }
    }
}