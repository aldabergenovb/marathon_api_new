using System;
using System.Collections.Generic;
using System.Linq;

namespace pmo_api.Models.Directories
{
    public class BasePagination<TData>
    {
        public int Records { get; set; }
        public int LastPage { get; set; }
        public int Last_page { get => this.LastPage; }
        public int CurrentPage { get; set; }
        public int LoadedCount { get; set; }
        public bool IsAll { get; set; }

        public List<TData> Data { get; set; } = new List<TData>();
    }

    public class Pagination<TView, TData> : BasePagination<TView>
    {
        public Pagination() { }

        public Pagination(Func<TData, TView> mapSelector, IQueryable<TData> data, IBasePaginationOptions options)
        {
            Records = data.Count();
            var itemList = data;
            if (options.IsInfiniteScroll)
            {
                itemList = itemList.Skip(options.LoadedCount).Take(options.Size);
                if (options.Size + options.LoadedCount >= Records)
                {
                    IsAll = true;
                    LoadedCount = Records;
                }
                else
                {
                    LoadedCount = options.LoadedCount + options.Size;
                }
            }
            else
            {
                itemList = itemList.Skip((options.Page - 1) * options.Size).Take(options.Size);
            }

            Data = itemList.Select(mapSelector).ToList();

            LastPage = (int)Math.Ceiling(this.Records / (double)options.Size);

            CurrentPage = options.Page;
        }

        public Pagination(Func<TData, TView> mapSelector, List<TData> data, IBasePaginationOptions options)
        {
            Records = data.Count;
            var itemList = data;

            if (options.IsInfiniteScroll)
            {
                itemList = (List<TData>)itemList.Skip(options.LoadedCount).Take(options.Size);

                if (options.Size + options.LoadedCount >= Records)
                {
                    IsAll = true;
                    LoadedCount = Records;
                }
                else
                {
                    LoadedCount = options.LoadedCount + options.Size;
                }
            }
            else
            {
                itemList = (List<TData>)itemList.Skip((options.Page - 1) * options.Size).Take(options.Size);
            }

            Data = itemList.Select(mapSelector).ToList();

            LastPage = (int)Math.Ceiling(this.Records / (double)options.Size);
            CurrentPage = options.Page;
        }
    }
}