using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace pmo_api.Models.Directories
{
    public class BaseDirectoryViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string NameKz { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class BaseDirectoryWithCodeViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string NameKz { get; set; }
        public bool IsDeleted { get; set; }
        public string Code { get; set; }

        public BaseDirectoryWithCodeViewModel() { }
        public BaseDirectoryWithCodeViewModel(string code, string name, string nameKz)
        {
            Code = code;
            Name = name;
            NameKz = nameKz;
        }
    }

    /// <summary>Базовый класс справочников</summary>
    public class BaseDirectoryModel
    {
        /// <summary>Код</summary>
        [Key]
        [Comment("Код")]
        public string Code { get; set; }
        /// <summary>Наименование на русском языке</summary>
        [Comment("Наименование на русском языке")]
        public string NameRu { get; set; }
        /// <summary>Наименование на казахском языке</summary>
        [Comment("Наименование на казахском языке")]
        public string NameKz { get; set; }
        /// <summary>Описание</summary>
        [Comment("Описание")]
        public string Description { get; set; }
        /// <summary>Цвет элемента для инфографики</summary>
        [Comment("Цвет элемента для инфографики")]
        public string Color { get; set; }
        public BaseDirectoryModel() { }
        public BaseDirectoryModel(string code, string nameRu, string nameKz, string description, string color)
        {
            Code = code;
            NameRu = nameRu;
            NameKz = nameKz;
            Description = description;
            Color = color;
        }
    }

    public class BaseDirectorySelectVM
    {
        public string Value { get; set; }
        public string Label { get; set; }
    }
}