namespace pmo_api.Models.Directories
{
    public class AntDropdown<TValue>
    {
        public string Label { get; set; }
        public TValue Value { get; set; }

        public AntDropdown() { }
        public AntDropdown(TValue value, string label)
        {
            Label = label;
            Value = value;
        }
    }

    public class AntDropdownViewModel
    {
        public string Label { get; set; }
        public string Value { get; set; }
        public bool? Disabled { get; set; }
    }

    public class ProjectStatusGroupDropdown
    {
        public string Label { get; set; }
        public string Value { get; set; }

        public ProjectStatusGroupDropdown() { }
        public ProjectStatusGroupDropdown(string value)
        {
            Label = char.ToUpper(value[0]) + value.ToLower()[1..];
            Value = value;
        }
    }
}