using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace pmo_api.Models.Directories
{
    public class BaseAction
    {
        [Key]
        [Comment("Код")]
        public string Code { get; set; }
        [Comment("Наименование на русском")]
        public string NameRu { get; set; }
        [Comment("Наименование на казахском")]
        public string NameKz { get; set; }
        [Comment("Наименование на английском")]
        public string NameEn { get; set; }
        [Comment("Роли для доступа")]
        public string[] AccessRoles { get; set; }
        public string[] CalculationTypes { get; set; }
        [Comment("Список наименований модальных окон")]
        public string[] ModalFieldCodes { get; set; }
    }

    public class BaseActionViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public IList<ModalField> ModalFields { get; set; } = new List<ModalField>();
    }
}