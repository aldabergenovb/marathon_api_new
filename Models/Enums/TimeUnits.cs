namespace pmo_api.Models.Enums
{
    public enum TimeUnits
    {
        Years, Months, Days, Hours, Minutes, Seconds, Milliseconds, Ticks
    }
}