using System.Text.Json.Serialization;
using Newtonsoft.Json.Converters;


namespace pmo_api.Models.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum MarathonEnum
    {
        ForestTrail = 0,
        Ishim = 1
    }
}