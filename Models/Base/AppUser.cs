using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using pmo_api.Models.Files;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;

namespace pmo_api.Models.Base
{
    public interface IBaseUser
    {
        string Email { get; set; }
        string LastName { get; set; }
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string PhoneNumber { get; set; }
        string PositionName { get; set; }
        string PositionNameKz { get; set; }
    }

    public class AppUser : IdentityUser<Guid>, IBaseUser
    {
        public AppUser()
        {
            this.Id = Guid.NewGuid();
        }
        public AppUser(AppUserViewModel model)
        {
            this.UserName = model.Email.ToLower().Trim();
            this.Email = model.Email.ToLower().Trim();
            this.EmailConfirmed = model.EmailConfirmed;
            this.LastName = model.LastName.Trim();
            this.FirstName = model.FirstName.Trim();
            this.MiddleName = model.MiddleName?.Trim();
            this.PositionName = model.PositionName?.Trim();
            this.PositionNameKz = model.PositionNameKz?.Trim();
            this.PhoneNumber = model.PhoneNumber?.Trim();
        }

        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string FullName => $"{this.LastName} {this.FirstName} {this.MiddleName}".Trim();

        public DateTimeOffset Created { get; set; } = DateTimeOffset.UtcNow;
        public DateTimeOffset? EmailConfirmedDate { get; set; }
        public bool IsDeactivated { get; set; }
        public string DeactivatedReasonCode { get; set; }
        public string DeactivatedReasonName { get; set; }
        public string DescriptionReason { get; set; }
        public DateTimeOffset? AbsenceStartDate { get; set; }
        public DateTimeOffset? AbsenceEndDate { get; set; }
        public string Password { get; set; }
        public virtual ICollection<AppUserRole> UserRoles { get; set; }
        public string PositionName { get; set; }
        public string PositionNameKz { get; set; }

        public void Update(AppUserViewModel model)
        {
            this.LastName = model.LastName.Trim();
            this.FirstName = model.FirstName.Trim();
            this.MiddleName = model.MiddleName?.Trim();
            this.PositionName = model.PositionName?.Trim();
            this.PositionNameKz = model.PositionNameKz?.Trim();
            this.PhoneNumber = model.PhoneNumber?.Trim();
        }

        public void Update(UdpateActivationEmployeeModel model)
        {
            this.IsDeactivated = model.IsDeactivated;
            this.DeactivatedReasonCode = model.IsDeactivated ? model.DeactivatedReasonCode : null;
            this.DeactivatedReasonName = model.IsDeactivated ? model.DeactivatedReasonName : null;
            this.DescriptionReason = model.IsDeactivated ? model.DescriptionReason : null;
            this.AbsenceStartDate = model.IsDeactivated ? model.AbsenceStartDate : null;
            this.AbsenceEndDate = model.IsDeactivated ? model.AbsenceEndDate : null;
        }

        public void UpdatePassword(AppUser model)
        {
            this.PasswordHash = model.PasswordHash;
        }
    }

    public class UdpateActivationEmployeeModel
    {
        public bool IsDeactivated { get; set; }
        public string DeactivatedReasonCode { get; set; }
        public string DeactivatedReasonName { get; set; }
        public string DescriptionReason { get; set; }
        public DateTimeOffset? AbsenceStartDate { get; set; }
        public DateTimeOffset? AbsenceEndDate { get; set; }
    }

    public class AppUserViewModel
    {
        public Guid Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string PositionName { get; set; }
        public string PositionNameKz { get; set; }
        public string Role { get; set; }
        public string OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationNameKz { get; set; }
        public bool EmailConfirmed { get; set; }
        public List<string> SupervisedOrganizationIds { get; set; } = new List<string>();
    }

    public class SupervisedOrganizationViewModel
    {
        public List<string> SupervisedOrganizationIds { get; set; } = new List<string>();
    }

    public class AppUserTestViewModel
    {
        public Guid Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string PositionName { get; set; }
        public string PositionNameKz { get; set; }
        public string Role { get; set; }
        public string OrganizationName { get; set; }
    }

    public class AppUserRegisterViewModel
    {
        public string Email { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string PositionName { get; set; }
        public string PositionNameKz { get; set; }
        public string Role { get; set; }
    }

    public class AppUserShortViewModel
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
    }

    public class PutAppUserForm
    {
        [Required]
        public string LastName { get; set; }

        [Required]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        public string ExtCode { get; set; }
        public string PositionName { get; set; }
        public string PositionNameKz { get; set; }
    }

    public class LoginForm
    {
        [Required]
        [EmailAddress]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public bool SavePassword { get; set; }
    }

    public class ConfirmEmailForm
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        public string Code { get; set; }
    }

    public class ForgotPasswordForm
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }

    public class ResetPasswordForm
    {
        [Required]
        public string UserId { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }
        public bool PasswordValidation() => !string.IsNullOrEmpty(this.Password) && (this.Password == this.PasswordConfirm);
    }

    public class ChangePasswordForm
    {
        [Required]
        public string CurrentPassword { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }
        public bool PasswordValidation() => !string.IsNullOrEmpty(this.Password) && (this.Password == this.PasswordConfirm);
    }

    public class AppUserPhoto
    {
        [Required]
        public virtual RemoteFile Photo { get; set; }

        [Key]
        [ForeignKey("AppUser")]
        public Guid AppUserId { get; set; }
        [Key]
        [ForeignKey("Photo")]
        public int PhotoId { get; set; }
        public AppUserPhoto() { }
        public AppUserPhoto(RemoteFile file, Guid userId)
        {
            AppUserId = userId;
            Photo = file;
        }
    }

    public class AppUserModalViewModel
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PhoneNumber { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
    }
}