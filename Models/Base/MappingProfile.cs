using System.Linq;
using AutoMapper;
using pmo_api.Constants;
using pmo_api.Models.Directories;
using System;
using pmo_api.Models.MarathonUserss;

namespace pmo_api.Models.Base
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            #region Users
            CreateMap<AppUser, AppUserViewModel>()
                .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.UserRoles.FirstOrDefault() == null ? null : src.UserRoles.FirstOrDefault().Role.Name))
            ;
            CreateMap<AppUser, AppUserShortViewModel>()
                .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.UserRoles.First() == null ? null : src.UserRoles.First().Role.Name ?? null))
            ;
            CreateMap<AppUser, AppUserModalViewModel>()
               ;
            #endregion

            CreateMap<MarathonUsers, MarathonUsersViewModel>()
               ;

            CreateMap<MarathonUsers, MarathonUsersMainPageViewModel>()
            .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.LastName + " " + src.FirstName));
            ;

            #region Directories
            CreateMap<BaseDirectoryViewModel, BaseDirectorySelectVM>()
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id.ToString()));
            CreateMap<BaseDirectoryWithCodeViewModel, BaseDirectoryViewModel>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => (!string.IsNullOrEmpty(src.Code) ? (src.Code + " - ") : "") + src.Name))
                .ForMember(dest => dest.NameKz, opt => opt.MapFrom(src => (!string.IsNullOrEmpty(src.Code) ? (src.Code + " - ") : "") + src.NameKz))
            ;
            CreateMap<AppUser, BaseDirectorySelectVM>()
                .ForMember(dest => dest.Label, opt => opt.MapFrom(src => src.LastName + " " + src.FirstName))
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id.ToString()))
            ;
            CreateMap<AppRole, BaseDirectorySelectVM>()
                .ForMember(dest => dest.Label, opt => opt.MapFrom(src => src.NameRu))
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Name))
            ;
            #endregion
        }
    }
}