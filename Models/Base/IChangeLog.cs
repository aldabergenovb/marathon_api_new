using System;

namespace pmo_api.Models.Base
{
    public interface IChangeLog
    {
        public int Id { get; set; }
        public DateTimeOffset LogDate { get; set; }
        public AppUser Author { get; set; }
        public string LogCode { get; set; }
        public string Description { get; set; }
    }

    public class ChangeLogViewModel
    {
        public int Id { get; set; }
        public DateTimeOffset LogDate { get; set; }
        public string AuthorFullName { get; set; }
        public string LogName { get; set; }
        public string Description { get; set; }
    }
}