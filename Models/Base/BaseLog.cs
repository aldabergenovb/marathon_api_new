using System;
using pmo_api.Models.Common;
using Microsoft.EntityFrameworkCore;

namespace pmo_api.Models.Base
{
    public class BaseLog : AuditableEntity
    {
        [Comment("Идентификатор лога")]
        public int Id { get; set; }
        [Comment("Дата лога")]
        public DateTimeOffset LogDate { get; set; }
        [Comment("Идентификатор пользователя")]
        public Guid AuthorId { get; set; }
        public virtual AppUser Author { get; set; }
        [Comment("Роль пользователя")]
        public string Role { get; set; }
        [Comment("Код")]
        public string LogCode { get; set; }
        [Comment("Причина")]
        public string Reason { get; set; }
    }
}