using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace pmo_api.Models.Base
{
    public class AppUserRole : IdentityUserRole<Guid>
    {
        public virtual AppUser User { get; set; }
        public virtual AppRole Role { get; set; }
    }

    public class AppUserRoleVM
    {
        public AppUser User { get; set; }
        public string Role { get; set; }
    }
}