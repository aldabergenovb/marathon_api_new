using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace pmo_api.Models.Base
{
    public class AppRole : IdentityRole<Guid>
    {
        public string NameRu { get; set; }
        public virtual ICollection<AppUserRole> UserRoles { get; set; }
    }
}