using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using pmo_api.Constants;


namespace pmo_api.Models.Base
{
    public static class SeedData
    {
        public static async void Initialize(IWebHost host)
        {
            using var scope = host.Services.CreateScope();
            var services = scope.ServiceProvider;

            try
            {
                var _db = services.GetRequiredService<AppDbContext>();
                var _userManager = services.GetRequiredService<UserManager<AppUser>>();
                var _roleManager = services.GetRequiredService<RoleManager<AppRole>>();

                // _db.Database.EnsureCreated();
                _db.Database.Migrate();

                #region Roles

                var roleList = UserConstants.AllRoles;

                var roles = _roleManager.Roles.Select(x => x.Name).ToList();
                // Сравнивает списки и разницу добавляет в БД
                var roleDiff = roleList.Except(roles).ToList();
                if (roleDiff.Any())
                {
                    // Связывает роли с входной группой
                    foreach (var item in roleDiff)
                    {
                        var role = new AppRole { Name = item };
                        var t = _roleManager.CreateAsync(role);
                        t.Wait();
                    }
                }
                #endregion

                #region USERS
                const string systemUserName = "system@vspace.kz";
                var systemUser = _db.Users.FirstOrDefault(x => x.UserName == systemUserName);
                if (systemUser is null)
                {
                    systemUser = new AppUser()
                    {
                        UserName = systemUserName,
                        Email = systemUserName,
                        LastName = "system",
                        FirstName = "user",
                        EmailConfirmed = true,
                        EmailConfirmedDate = DateTimeOffset.UtcNow
                    };

                    var systemCreation = _userManager.CreateAsync(systemUser, "P@ssw0rd");
                    systemCreation.Wait();
                    var systemRole = _userManager.AddToRoleAsync(systemUser, UserConstants.Roles.System);
                    systemRole.Wait();
                }

                const string rootUserName = "root@aqmola-marathon.kz";
                var rootUser = _db.Users.FirstOrDefault(x => x.UserName == rootUserName);
                if (rootUser is null)
                {
                    rootUser = new AppUser()
                    {
                        UserName = rootUserName,
                        Email = rootUserName,
                        LastName = "root",
                        FirstName = "user",
                        EmailConfirmed = true,
                        EmailConfirmedDate = DateTimeOffset.UtcNow
                    };

                    var rootCreation = _userManager.CreateAsync(rootUser, "P@ssw0rd");
                    rootCreation.Wait();
                    var rootRole = _userManager.AddToRoleAsync(rootUser, UserConstants.Roles.Root);
                    rootRole.Wait();
                }

                var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                if (env != "Production")
                {
                    var employeeRoles = UserConstants.EmployeeRoles;
                    var userRoles = _db.Users.Select(x => x.UserRoles.FirstOrDefault().Role.Name).ToList();
                    // Сравнивает списки и разницу добавляет в БД
                    var employeeRoleDiff = employeeRoles.Except(userRoles).ToList();
                    if (employeeRoleDiff.Any())
                    {
                        // Связывает роли с входной группой
                        foreach (var role in employeeRoleDiff)
                        {
                            var email = role.ToLower() + "@vspace.kz";
                            var user = _db.Users.FirstOrDefault(x => x.UserName == email);
                            if (user == null)
                            {
                                user = new AppUser()
                                {
                                    UserName = email,
                                    Email = email,
                                    LastName = role,
                                    FirstName = role,
                                    EmailConfirmed = true,
                                    EmailConfirmedDate = DateTimeOffset.UtcNow
                                };

                                var userCreation = _userManager.CreateAsync(user, "P@ssw0rd");
                                userCreation.Wait();
                                var userRole = _userManager.AddToRoleAsync(user, role);
                                userRole.Wait();
                            }
                        }
                    }
                }
                #endregion
                await _db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }
    }
}