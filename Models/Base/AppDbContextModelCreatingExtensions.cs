using Microsoft.EntityFrameworkCore;


namespace pmo_api.Models.Base
{
    public static class AppDbContextModelCreatingExtensions
    {
        public static void ConfigureApp(this ModelBuilder builder)
        {
            #region Base
            builder.Entity<AppUser>()
                .HasMany(u => u.UserRoles)
                .WithOne(ur => ur.User)
                .HasForeignKey(ur => ur.UserId)
                .IsRequired();

            builder.Entity<AppUserPhoto>()
                .HasKey(x => new { x.AppUserId, x.PhotoId });

            builder.Entity<AppRole>()
                .HasMany(u => u.UserRoles)
                .WithOne(ur => ur.Role)
                .HasForeignKey(ur => ur.RoleId)
                .IsRequired();

            builder.Entity<AppUser>()
                .HasMany(u => u.UserRoles)
                .WithOne(ur => ur.User)
                .HasForeignKey(ur => ur.UserId)
                .IsRequired();
            #endregion

            builder.Entity<RefreshToken>()
                .Property(x => x.StartDate).HasColumnType("timestamp without time zone");

            builder.Entity<RefreshToken>()
                .Property(x => x.FinishDate).HasColumnType("timestamp without time zone");
        }
    }
}