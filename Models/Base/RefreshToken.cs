using System;

namespace pmo_api.Models.Base
{
    public class RefreshToken
    {
        public RefreshToken() { }
        public RefreshToken(Guid _id) { Id = _id; }
        public Guid Id { get; set; } // = AppUser.Id
        public string Token { get; set; }
        public bool Revoked { get; set; }
        public DateTime StartDate { get; set; } // = Created
        public DateTime FinishDate { get; set; } // = ExpireDate
    }
}