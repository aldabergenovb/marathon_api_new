using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using pmo_api.Models.Directories;
using pmo_api.Models.Files;
using pmo_api.Models.Users;
using pmo_api.Services;
using pmo_api.Models.Common;
using pmo_api.Models.MarathonUserss;

namespace pmo_api.Models.Base
{
    public interface IAppDbContext { }
    public class AppDbContext :
        IdentityDbContext<AppUser, AppRole, Guid, IdentityUserClaim<Guid>,
            AppUserRole, IdentityUserLogin<Guid>, IdentityRoleClaim<Guid>, IdentityUserToken<Guid>>
        , IAppDbContext
    {
        private readonly ICurrentUserService _currentUserService;
        public AppDbContext(DbContextOptions<AppDbContext> options, ICurrentUserService currentUserService) : base(options)
        {
            _currentUserService = currentUserService;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsContractor)
        {
            optionsContractor.EnableSensitiveDataLogging();
        }

        protected override void ConfigureConventions(ModelConfigurationBuilder configurationContractor)
        {
            configurationContractor
                .Properties<DateTimeOffset>()
                .HaveConversion<DateTimeOffsetConverter>();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ConfigureApp();
        }

        #region Base
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        #endregion

        #region Users
        public DbSet<AppUser> AppUsers { get; set; }
        #endregion

        #region Logs
        public DbSet<AppUserLog> AppUserLogs { get; set; }
        #endregion

        #region Views
        public DbSet<BaseDirectoryViewModel> BaseDirectoryViewModels { get; set; }    // dashboards view
        public DbSet<BaseDirectoryWithCodeViewModel> BaseDirectoryWithCodeViewModels { get; set; }    // dashboards view
        #endregion

        #region File
        public DbSet<RemoteFile> RemoteFiles { get; set; }
        public DbSet<AppUserPhoto> AppUserPhotos { get; set; }
        #endregion
        public DbSet<MarathonUsers> MarathonUsers { get; set; }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            var deleteableEntityTypes = new List<string>() { "ProjectPositionProxy", "TaskPositionProxy" };
            foreach (var entry in ChangeTracker.Entries<AuditableEntity>())
            {
                var systemUser = await base.Users.FirstOrDefaultAsync(x => string.Equals(x.UserName, "system@vspace.kz"), cancellationToken: cancellationToken);
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedById = _currentUserService.UserId ?? systemUser.Id;
                        entry.Entity.Created = DateTimeOffset.UtcNow;
                        break;

                    case EntityState.Modified:
                        entry.Entity.LastModifiedById = _currentUserService.UserId ?? (Guid?)systemUser.Id;
                        entry.Entity.LastModified = DateTimeOffset.UtcNow;
                        break;
                    case EntityState.Deleted:
                        entry.Entity.DeletedById = _currentUserService.UserId ?? (Guid?)systemUser.Id;
                        var entityType = entry.Entity.GetType();
                        entry.Entity.Deleted = DateTimeOffset.UtcNow;
                        entry.Entity.IsDeleted = true;
                        if (!deleteableEntityTypes.Any(x => x == entityType.Name))
                            entry.State = EntityState.Modified;
                        break;
                }
            }

            var result = await base.SaveChangesAsync(cancellationToken);
            return result;
        }
    }

    public class DateTimeOffsetConverter : ValueConverter<DateTimeOffset, DateTimeOffset>
    {
        public DateTimeOffsetConverter() : base(d => d.ToUniversalTime(), d => d.ToUniversalTime()) { }
    }
}