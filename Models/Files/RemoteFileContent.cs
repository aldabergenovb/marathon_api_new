using System;
using pmo_api.Constants;

namespace pmo_api.Models.Files
{
    public class RemoteFileContent : RemoteFileBaseModel
    {
        public RemoteFileContent() { }
        public Guid Id { get; set; } = Guid.NewGuid();
        public byte[] Content { get; set; }
    }
}