using System;
using pmo_api.Models.Base;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace pmo_api.Models.Files
{
    public class RemoteFileBaseModel
    {
        public string Name { get; set; }
        public string Extension { get; set; }
        public double Size { get; set; }
        public DateTimeOffset? UploadedDate { get; set; } = DateTimeOffset.UtcNow;
        public string Status { get; set; }
        public Guid? UploadedById { get; set; }
        public string UploadedByRole { get; set; }
    }

    /// <summary>
    /// max file size 10mb, max total file size 50mb
    /// </summary>
    public class RemoteFile : RemoteFileBaseModel
    {
        public RemoteFile() { }
        public RemoteFile(RemoteFileContent content)
        {
            this.Extension = content.Extension;
            this.Name = content.Name;
            this.RemoteFileContentId = content.Id;
            this.Size = content.Size;
            this.Status = content.Status;
            this.UploadedById = content.UploadedById;
            this.UploadedByRole = content.UploadedByRole;
            this.UploadedDate = content.UploadedDate;
        }

        public int Id { get; set; }
        public Guid? RemoteFileContentId { get; set; }

        public RemoteFileShortViewModel ToShortViewModel()
        {
            return new RemoteFileShortViewModel()
            {
                Id = this.Id,
                Name = this.Name,
                UploadedByRole = this.UploadedByRole,
                RemoteFileContentId = this.RemoteFileContentId,
            };
        }

        public RemoteFileShortViewModel ToShortViewModel(bool isMain)
        {
            return new RemoteFileShortViewModel()
            {
                Id = this.Id,
                Name = this.Name,
                UploadedByRole = this.UploadedByRole,
                RemoteFileContentId = this.RemoteFileContentId,
                IsMain = isMain
            };
        }
        public RemoteFileShortViewModel ToShortViewModel(string documentCode)
        {
            return new RemoteFileShortViewModel()
            {
                Id = this.Id,
                Name = this.Name,
                UploadedByRole = this.UploadedByRole,
                DocumentTypeCode = documentCode,
            };
        }
    }

    public class RemoteFileShortViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UploadedByRole { get; set; }
        public string DocumentTypeCode { get; set; }
        public Guid? RemoteFileContentId { get; set; }
        public FileContentResult Content { get; set; }
        public bool? IsMain { get; set; }
    }

    public interface ITypedRemoteFileForm
    {
        /// <summary>
        /// The user who wants to upload a file.
        /// </summary>
        AppUser Author { get; set; }

        /// <summary>Uploading file type</summary>
        string Type { get; set; }

        /// <summary>Uploading file</summary>
        IFormFile File { get; set; }
    }
}