namespace pmo_api.Models.Files
{
    public class FileBase64Content
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public double Size { get; set; }
        public string Base64string { get; set; }
        public string Extension { get; set; }
    }
}