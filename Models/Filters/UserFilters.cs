using System;
using System.Collections.Generic;
using pmo_api.Helpers;
using pmo_api.Models.Directories;

namespace pmo_api.Models.Filters
{
    public class UserFilters : BasePaginationOptions<AppUsersSortState>
    {
        public UserFilters() : base(AppUsersSortState.LastNameAsc) { }

        public string Email { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PositionName { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationId { get; set; }
        public string OrganizationIdOmit { get; set; }
        public string PositionNameKz { get; set; }
        public List<string> Role { get; set; }
        public string Created { get; set; }
        public DateTimeOffset? StartCreated { get; set; }
        public DateTimeOffset? FinishCreated { get; set; }
    }
}