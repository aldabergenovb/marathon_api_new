using System;

namespace pmo_api.Models.Filters
{
    public class ChangeLogFilters : Directories.BasePaginationOptions<ChangeLogSortState>
    {
        public ChangeLogFilters() : base(ChangeLogSortState.IdDesc) { }

        public DateTimeOffset? LogDateBefore { get; set; }
        public DateTimeOffset? LogDateAfter { get; set; }
        public string AuthorFullName { get; set; }
        public string Description { get; set; }
    }

    public enum ChangeLogSortState
    {
        IdAsc, LogDateAsc, AuthorFullNameAsc, DescriptionAsc,
        IdDesc, LogDateDesc, AuthorFullNameDesc, DescriptionDesc,
    }
}