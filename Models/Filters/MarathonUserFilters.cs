using System;
using pmo_api.Models.Enums;
using System.Collections.Generic;
using static pmo_api.Helpers.MarathonUsersHelper;

namespace pmo_api.Models.Filters
{
    public class MarathonUsersFilters : Directories.BasePaginationOptions<MarathonUsersSortState>
    {
        public MarathonUsersFilters() : base(MarathonUsersSortState.CreatedDateAsc) { }

        public DateTimeOffset? CreatedBefore { get; set; }
        public DateTimeOffset? CreatedAfter { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public string GenderName { get; set; }
        public string CountryName { get; set; }
        public string CityName { get; set; }
        public string ExtraContact { get; set; }
        new public string Size { get; set; }
        public string Distance { get; set; }
        public MarathonEnum marathonEnum { get; set; }
    }
}