using System;

namespace pmo_api.Models.Users
{
    public class AppUserLog
    {
        public AppUserLog() { }

        public AppUserLog(string action, Guid authorId, string authorUserName, string authorRole, string email, string role) : base()
        {
            Action = action;
            AuthorId = authorId;
            AuthorUserName = authorUserName;
            AuthorRole = authorRole;
            Email = email;
            Role = role;
        }

        public int Id { get; set; }
        public DateTimeOffset Created { get; set; } = DateTimeOffset.UtcNow;
        public string Action { get; set; }  // "create", update
        public Guid AuthorId { get; set; }
        public string AuthorUserName { get; set; }
        public string AuthorRole { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }

        public static AppUserLog InstanceCreateLog(
                Guid authorId, string authorUserName, string authorRole,
                string email, string role
            )
        {
            return new AppUserLog("create", authorId, authorUserName, authorRole, email, role);
        }

        public static AppUserLog InstanceLog(
                string action,
                Guid authorId, string authorUserName, string authorRole,
                string email, string role
            )
        {
            return new AppUserLog(action, authorId, authorUserName, authorRole, email, role);
        }

        public static AppUserLog InstanceUpdateLog(
                Guid authorId, string authorUserName, string authorRole,
                string email, string role
            )
        {
            return new AppUserLog("update", authorId, authorUserName, authorRole, email, role);
        }
    }
}