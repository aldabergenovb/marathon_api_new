using System;

namespace pmo_api.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string message) : base(message) { }
        public NotFoundException(string name, object key) : base($"Значение \"{name}\" ({key}) не найдено") { }

        public NotFoundException() { }

        public NotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}