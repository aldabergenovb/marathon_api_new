using System;
using System.Threading.Tasks;
using System.Net;
using Microsoft.AspNetCore.Http;
using pmo_api.Models.Directories;
using pmo_api.Constants;
using pmo_api.Exceptions;
using Newtonsoft.Json;

namespace pmo_api
{
    public class CustomExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public CustomExceptionHandlerMiddleware(RequestDelegate next) =>
            _next = next;

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception exception)
            {
                await HandleExceptionAsync(context, exception);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var code = HttpStatusCode.InternalServerError;
            var result = string.Empty;
            switch (exception)
            {
                case ServerErrorException:
                    code = HttpStatusCode.OK;
                    result = JsonConvert.SerializeObject(ActionMethodResult.Error(ActionMethodConstants.Results.ServerError, "", "Ошибка интеграционного сервиса"));
                    break;
                case UserCreatingException:
                    code = HttpStatusCode.OK;
                    result = JsonConvert.SerializeObject(ActionMethodResult.Error(ActionMethodConstants.Results.ServerError, "", "Ошибка интеграционного сервиса"));
                    break;
                case NotFoundException:
                    code = HttpStatusCode.OK;
                    result = JsonConvert.SerializeObject(ActionMethodResult.Error(ActionMethodConstants.Results.ServerError, "", exception.Message));
                    break;
                case AccessDeniedException:
                    code = HttpStatusCode.OK;
                    result = JsonConvert.SerializeObject(ActionMethodResult.Error(ActionMethodConstants.Results.AccessDenied, "", exception.Message));
                    break;
                case Exception:
                    code = HttpStatusCode.OK;
                    result = JsonConvert.SerializeObject(ActionMethodResult.Error(ActionMethodConstants.Results.ServerError, "", $"{exception.Message}"));
                    break;
            }
            System.Console.WriteLine(exception.Message);
            System.Console.WriteLine(exception.StackTrace);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }
}