using Microsoft.AspNetCore.Builder;

namespace pmo_api
{
    public static class CustomExceptionHandlerExtension
    {
        public static IApplicationBuilder UseCustomExceptionHandler(this
            IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CustomExceptionHandlerMiddleware>();
        }
    }
}