using System;
using System.Collections.Generic;
using System.Linq;

namespace pmo_api.Hubs
{
    public class VoipHubConnection
    {
        private readonly Dictionary<Guid, HashSet<string>> _connections = new();

        public int Count
        {
            get { return _connections.Count; }
        }

        public void Add(Guid key, string model)
        {
            lock (_connections)
            {
                if (!_connections.TryGetValue(key, out HashSet<string> connections))
                {
                    connections = new HashSet<string>();
                    _connections.Add(key, connections);
                }

                lock (connections)
                {
                    connections.Add(model);
                }
            }
        }

        public IEnumerable<string> GetConnections(Guid key)
        {
            if (_connections.TryGetValue(key, out HashSet<string> connections))
                return connections;

            return Enumerable.Empty<string>();
        }

        public IEnumerable<string> GetConnections()
        {
            HashSet<string> connections = new();
            foreach (var item in _connections.Values)
            {
                connections = (HashSet<string>)connections.Concat(item);
            }
            return connections;
        }

        public IEnumerable<string> GetConnectionsExceptKey(Guid key)
        {
            HashSet<string> connections = new();
            foreach (var item in _connections.Where(x => x.Key != key).ToDictionary(x => x.Key, x => x.Value).Values)
            {
                connections = (HashSet<string>)connections.Concat(item);
            }
            return connections;
        }

        public void Remove(Guid key, string model)
        {
            lock (_connections)
            {
                if (!_connections.TryGetValue(key, out HashSet<string> connections))
                    return;

                lock (connections)
                {
                    connections.Remove(model);
                    if (connections.Count == 0)
                        _connections.Remove(key);
                }
            }
        }
    }
}