using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using pmo_api.Models.Base;
using System.Collections.Generic;

namespace pmo_api.Hubs
{
    [Authorize]
    public class VoipHub : Hub
    {
        private readonly UserManager<AppUser> _userManager;
        public readonly static VoipHubConnection _connections = new();

        public VoipHub(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }

        public override async Task OnConnectedAsync()
        {
            var user = await _userManager.GetUserAsync(Context.User);
            if (user != null)
            {
                if (!_connections.GetConnections(user.Id).Any(x => x == Context.ConnectionId))
                {
                    var httpContext = Context.GetHttpContext();
                    _connections.Add(user.Id, Context.ConnectionId);
                    Console.WriteLine($"VoipHub connect: {user?.UserName} | {Context.ConnectionId}");
                    await Clients.Caller.SendAsync("SendAction", Context.ConnectionId, "connected");
                }
                else
                {
                    Console.WriteLine($"VoipHub reconnect: {user?.UserName} | {Context.ConnectionId}");
                    await Clients.Caller.SendAsync("SendAction", Context.ConnectionId, "reconnected");
                }
            }
        }

        public override async Task OnDisconnectedAsync(Exception ex)
        {
            var user = await _userManager.GetUserAsync(Context.User);
            if (user != null)
            {
                _connections.Remove(user.Id, Context.ConnectionId);
                Console.WriteLine($"VoipHub disconnect: {user?.UserName} | {Context.ConnectionId}");
                await Clients.Caller.SendAsync("SendAction", Context.ConnectionId, "disconnected");
            }
        }

        public static IEnumerable<string> GetConnections(Guid key)
        {
            return _connections.GetConnections(key);
        }
    }
}