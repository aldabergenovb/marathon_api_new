using System;
using System.Threading.Tasks;
using pmo_api.Models.Base;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;

namespace pmo_api.Hubs
{
    [Authorize]
    public class UnseenRequestsHub : Hub
    {
        private readonly UserManager<AppUser> _userManager;
        public UnseenRequestsHub(UserManager<AppUser> userManager)
        {
            _userManager = userManager;
        }

        public override async Task OnConnectedAsync()
        {
            var user = await _userManager.GetUserAsync(Context.User);
            if (user != null)
            {
                var message = $"user {user?.UserName} requests unseenRequests, ConnectionId: {Context.ConnectionId}";
                Console.WriteLine(message);
                await Clients.Caller.SendAsync("SendAction", Context.ConnectionId, message);
                await base.OnConnectedAsync();
            }
        }

        public override async Task OnDisconnectedAsync(Exception ex)
        {
            var user = await _userManager.GetUserAsync(Context.User);
            if (user != null)
            {
                var httpContext = Context.GetHttpContext();
                var queryParams = httpContext.Request.Query["code"];
                if (queryParams.Count == 1)
                {
                    var message = $"{user?.UserName} with connectionId: {Context.ConnectionId} left";
                    await Clients.Caller.SendAsync("SendAction", Context.ConnectionId, message);
                    await base.OnDisconnectedAsync(ex);
                }
            }
        }
    }
}